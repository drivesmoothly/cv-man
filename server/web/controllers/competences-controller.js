"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    var Cv = opts.Cv || require("../../models/cv");
    var User = opts.User || require("../../models/user");
    var Competence = opts.Competence || require("../../models/competence");
    var json_competence = opts.json_coompetence || require("./json-formatters/json-competence")(opts);

    function get_root(req, res) {
        var skip = req.headers.skip || 0;
        var limit = req.headers.limit || 100;
        limit = Math.min(100, limit);

        Competence.find(req.query, "id cv_id skill_id experience", {skip: skip, limit: limit}, function (err, dbCompetences) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid object id format"});
                }
                return res.status(500).json({success: false, message: "Cannot read Competences from DB."});
            }

            var competences = [];
            dbCompetences.forEach(function (dbCompetence) {
                competences.push(json_competence.jsonCompetence(dbCompetence));
            });

            return res.status(200).json(competences);
        });
    }

    function post_root(req, res) {
        // make sure we have a CV id and a Skill id
        if (!req.body.cv_id) {
            return res.status(400).json({success: false, message: "CV ID is mandatory"});
        }
        if (!req.body.skill_id) {
            return res.status(400).json({success: false, message: "Skill ID is mandatory"});
        }

        // Find a competence with the same skill
        Competence.find({cv_id: req.body.cv_id, skill_id: req.body.skill_id}, "id", function (err, dbDuplicate) {
            if (err) {
                return res.stauts(500).json({success: false, message: "Cannot read competences from DB."});
            }
            if (dbDuplicate.length) {
                return res.status(409).json({
                    success: false,
                    message: "A competence for this skill already exists",
                    links: {
                        duplicate: opts.baseURL + "/api/competences/" + dbDuplicate[0].id
                    }
                });
            }

            var competence = new Competence({
                cv_id: req.body.cv_id,
                skill_id: req.body.skill_id,
                experience: req.body.experience
            });

            competence.save(function (err) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot save competence to DB."});
                }
                return res.status(200).json(json_competence.jsonCompetence(competence));
            });
        });
    }

    function get_compid(req, res) {
        Competence.findById(req.params.id, "id cv_id skill_id experience", function (err, dbCompetence) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid ID format"});
                }
                return res.status(500).json({success: false, message: "Cannot read competences from DB."});
            }

            if (!dbCompetence) {
                return res.status(404).json({success: false, message: "Cannot find competence."});
            }

            return res.status(200).json(json_competence.jsonCompetence(dbCompetence));
        });
    }

    function put_compid(req, res) {
        Competence.findById(req.params.id, "id cv_id skill_id experience", function (err, dbCompetence) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid ID format"});
                }
                return res.status(500).json({success: false, message: "Cannot read competences from DB."});
            }

            if (!dbCompetence) {
                return res.status(404).json({success: false, message: "Cannot find competence."});
            }

            dbCompetence.cv_id = req.body.cv_id || dbCompetence.cv_id;
            dbCompetence.skill_id = req.body.skill_id || dbCompetence.skill_id;
            dbCompetence.experience = req.body.experience || dbCompetence.experience;

            dbCompetence.save(function (err) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot save competence."});
                }
                return res.status(200).json(json_competence.jsonCompetence(dbCompetence));
            });
        });
    }

    function delete_compid(req, res) {
        Competence.findById(req.params.id, "id", function (err, dbCompetence) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid ID format"});
                }
                return res.status(500).json({success: false, message: "Cannot read competences from DB."});
            }

            if (!dbCompetence) {
                return res.status(404).json({success: false, message: "Cannot find competence."});
            }

            dbCompetence.remove(function (err) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot delete competence."});
                }
                return res.status(200).json({success: true, message: "Competence deleted."});
            });
        });
    }

    return {
        get_root: get_root,
        post_root: post_root,

        get_compid: get_compid,
        put_compid: put_compid,
        delete_compid: delete_compid
    };
}

module.exports = moduleFunction;
