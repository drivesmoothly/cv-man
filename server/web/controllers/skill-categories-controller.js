"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    var SkillCategory = opts.SkillCategory || require("../../models/skill-category");
    var jsonSkillCategory = opts.jsonSkillCategory || require("../routes/json-formatters/json-skill-category")(opts);

    function root_get(req, res) {
        var skip = req.headers.skip || 0;
        var limit = req.headers.limit || 100;
        limit = Math.min(100, limit);

        SkillCategory.find(req.query, "id name path", {skip: skip, limit: limit}, function (err, dbCategories) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid object id format."});
                }
                return res.status(500).json({success: false, message: "Cannot read categories from DB."});
            }

            var categories = [];
            dbCategories.forEach(function (dbCategory) {
                categories.push(jsonSkillCategory.jsonSkillCategory(dbCategory));
            });

            return res.status(200).json(categories);
        });
    }

    function root_post(req, res) {
        // validate the necessary arguments
        if (!req.body.name) {
            return res.status(400).json({success: false, message: "Category name not provided"});
        }

        var category = new SkillCategory({name: req.body.name, path: req.body.path || ""});
        category.save(function (err) {
            if (err) {
                return res.status(500).json({success: false, message: "Cannot save category to DB."});
            }
            res.status(200).json(jsonSkillCategory.jsonSkillCategory(category));
        });
    }

    function get_id(req, res) {
        SkillCategory.findById(req.params.id, "id name path", function (err, dbCategory) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid ID format."});
                }
                return res.status(500).json({success: false, message: "Cannot read category from DB."});
            }
            if (!dbCategory) {
                return res.status(404).json({success: false, message: "Cannot find category."});
            }
            res.status(200).json(jsonSkillCategory.jsonSkillCategory(dbCategory));
        });
    }

    function put_id(req, res) {
        SkillCategory.findById(req.params.id, "id name path", function (err, dbCategory) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid ID format."});
                }
                return res.status(500).json({success: false, message: "Cannot read category from DB."});
            }
            if (!dbCategory) {
                return res.status(404).json({success: false, message: "Cannot find category"});
            }
            dbCategory.name = req.body.name || dbCategory.name;
            dbCategory.path = req.body.path || dbCategory.path;

            dbCategory.save(function (err) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot save category to DB."});
                }
                return res.status(200).json(jsonSkillCategory.jsonSkillCategory(dbCategory));
            });
        });
    }

    function delete_id(req, res) {
        SkillCategory.findById(req.params.id, "id", function (err, dbCategory) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid ID format."});
                }
                return res.status(500).json({success: false, message: "Cannot read category from DB."});
            }
            if (!dbCategory) {
                return res.status(404).json({success: false, message: "Cannot find category"});
            }

            dbCategory.remove(function (err) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot delete category from DB."});
                }
                return res.status(200).json({success: true, message: "The category was removed"});
            });
        });
    }

    function get_id_children(req, res) {
        SkillCategory.findById(req.params.id, "id name path", function (err, dbParent) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid ID format."});
                }
                return res.status(500).json({success: false, message: "Cannot read category from DB."});
            }
            if (!dbParent) {
                return res.status(404).json({success: false, message: "Cannot find category"});
            }

            var parentPath = dbParent.path + dbParent.name + ",";
            SkillCategory.find({path: parentPath}, "id name path", function (err, dbChildren) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot read categories from DB."});
                }
                var children = [];
                dbChildren.forEach(function (dbChild) {
                    children.push(jsonSkillCategory.jsonSkillCategory(dbChild));
                });
                return res.status(200).json(children);
            });
        });
    }

    return {
        root_get: root_get,
        root_post: root_post,
        get_id: get_id,
        put_id: put_id,
        delete_id: delete_id,
        get_id_children: get_id_children
    };
}

module.exports = moduleFunction;
