"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    var Skill = opts.Skill || require("../../models/skill");
    var SkillCategory = opts.SkillCategory || require("../../models/skill-category");
    var json_skill = opts.json_skill || require("../routes/json-formatters/json-skill")(opts);

    function get_root(req, res) {
        var skip = req.headers.skip || 0;
        var limit = req.headers.limit || 100;
        limit = Math.min(100, limit);

        Skill.find({}, "id name category_id", {skip: skip, limit: limit}, function (err, dbSkill) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid object id format."});
                }
                return res.status(500).json({success: false, message: "Cannot read skills from DB."});
            }

            var skills = [];
            dbSkill.forEach(function (dbSkill) {
                skills.push(json_skill.jsonSkill(dbSkill));
            });

            return res.status(200).json(skills);
        });
    }

    function post_root(req, res) {
        // check that we have a name
        if (!req.body.name) {
            return res.status(400).json({success: false, message: "Cannot create a skill without a name"});
        }

        // check if the category exists
        SkillCategory.findById(req.body.category_id, "id", function (err, dbCategory) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid category ID format."});
                }
                return res.status(500).json({success: false, message: "Cannot read categories from DB."});
            }
            if (req.body.category_id && !dbCategory) {
                return res.status(404).json({success: false, message: "Cannot find category."});
            }

            var skill = new Skill({
                name: req.body.name,
                category_id: req.body.category_id
            });

            skill.save(function (err) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot save skill to db."});
                }
                return res.status(200).json(json_skill.jsonSkill(skill));
            });
        });
    }

    function get_id(req, res) {
        Skill.findById(req.params.id, "id name category", function (err, dbSkill) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid ID format."});
                }
                return res.status(500).json({success: false, message: "Cannot read skill from DB."});
            }

            if (!dbSkill) {
                return res.status(404).json({success: false, message: "Cannot find skill."});
            }

            return res.status(200).json(json_skill.jsonSkill(dbSkill));
        });
    }

    function put_id(req, res) {
        Skill.findById(req.params.id, "id name category", function (err, dbSkill) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid ID format."});
                }
                return res.status(500).json({success: false, message: "Cannot read skill from DB."});
            }

            if (!dbSkill) {
                return res.status(404).json({success: false, message: "Cannot find skill."});
            }

            dbSkill.name = req.body.name || dbSkill.name;
            dbSkill.category_id = req.body.category_id || dbSkill.category_id;

            dbSkill.save(function (err) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot save skill to DB."});
                }
                return res.status(200).json(json_skill.jsonSkill(dbSkill));
            });
        });
    }

    function delete_id(req, res) {
        Skill.findById(req.params.id, "id name category", function (err, dbSkill) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid ID format."});
                }
                return res.status(500).json({success: false, message: "Cannot read skill from DB."});
            }

            if (!dbSkill) {
                return res.status(404).json({success: false, message: "Cannot find skill."});
            }

            dbSkill.remove(function (err) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot delete skill from DB."});
                }
                return res.status(200).json({success: true, message: "Skill removed"});
            });
        });
    }

    return {
        root_get: get_root,
        root_post: post_root,
        get_id: get_id,
        put_id: put_id,
        delete_id: delete_id
    };
}

module.exports = moduleFunction;
