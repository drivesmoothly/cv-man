"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    function jsonCompetence(dbCompetence) {
        return {
            id: dbCompetence.id,
            cv_id: dbCompetence.cv_id,
            skill_id: dbCompetence.skill_id,
            experience: {
                months: dbCompetence.experience.months,
                since: dbCompetence.experience.since,
                percentage: dbCompetence.experience.percentage
            },
            links: {
                self: opts.baseURL + "/api/competences/" + dbCompetence.id,
                cv: opts.baseURL + "/api/cvs/" + dbCompetence.cv_id,
                skill: opts.baseURL + "/api/skills/" + dbCompetence.skill_id
            }
        };
    }

    return {
        jsonCompetence: jsonCompetence
    };
}

module.exports = moduleFunction;
