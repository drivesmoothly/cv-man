"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    var Cv = opts.Cv || require("../../models/cv");
    var User = opts.User || require("../../models/user");
    var json_cv = opts.json_cv || require("../routes/json-formatters/json-cv")(opts);
    var competencesController = opts.competencesController || require("./competences-controller")(opts);

    function root_get(req, res) {
        var skip = req.headers.skip || 0;
        var limit = req.headers.limit || 100;
        limit = Math.min(100, limit);

        Cv.find(req.query, "id name user_id job_title", {skip: skip, limit: limit}, function (err, dbCvs) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid object id format"});
                }
                return res.status(500).json({success: false, message: "Cannot read CVs from DB."});
            }
            var cvs = [];
            dbCvs.forEach(function (dbCv) {
                cvs.push(json_cv.jsonCv(dbCv));
            });
            return res.status(200).json(cvs);
        });
    }

    function root_post(req, res) {
        // validate the necessary arguments
        if (!req.body.name || !req.body.user_id) {
            return res.status(400).json({success: false, message: "CV name or user id not provided"});
        }

        // make sure we have the user
        User.findById(req.body.user_id, "id", function (err, user) {
            // find the user
            if (err) {
                return res.status(500).json({success: false, message: "Cannot read users from DB."});
            }
            if (!user) {
                return res.status(404).json({success: false, message: "User not found"});
            }

            // make sure it's unique for the user
            Cv.find({name: req.body.name, user_id: req.body.user_id}, "id", function (err, existingCvs) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot read CVs from the DB."});
                }
                if (existingCvs.length > 0) {
                    return res.status(409).json({
                        success: false,
                        message: "A CV with the same name already exists",
                        links: {
                            duplicate: opts.baseURL + "/api/cvs/" + existingCvs[0].id
                        }
                    });
                }

                // create the new CV
                var cv = new Cv({
                    name: req.body.name,
                    user_id: req.body.user_id,
                    job_title: req.body.job_title
                });

                cv.save(function (err) {
                    if (err) {
                        return res.status(500).json({success: false, message: "Cannot save the CV"});
                    }
                    return res.status(200).json(json_cv.jsonCv(cv));
                });
            });
        });
    }

    function get_cvid(req, res) {
        Cv.findById(req.params.id, "id name user_id job_title office_location competences", function (err, dbCv) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Wrong ID format"});
                }
                return res.status(500).json({success: false, message: "Cannot read CV from DB"});
            }
            if (!dbCv) {
                return res.status(404).json({success: false, message: "Cannot find CV"});
            }
            return res.status(200).json(json_cv.jsonDetailedCv(dbCv));
        });
    }

    function put_cvid(req, res) {
        Cv.findById(req.params.id, "id name user_id job_title", function (err, dbCv) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid ID format"});
                }
                return res.status(500).json({success: false, message: "Cannot read CV from DB."});
            }
            if (!dbCv) {
                return res.status(404).json({success: false, message: "Cannot find CV."});
            }
            dbCv.name = req.body.name || dbCv.name;
            dbCv.job_title = req.body.job_title || dbCv.job_title;

            dbCv.save(function (err) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot save CV to DB."});
                }
                return res.status(200).json(json_cv.jsonDetailedCv(dbCv));
            });
        });
    }

    function delete_cvid(req, res) {
        Cv.findById(req.params.id, "id", function (err, dbCv) {
            if (err) {
                if (err.kind === "ObjectId") {
                    return res.status(400).json({success: false, message: "Invalid ID format"});
                }
                return res.status(500).json({success: false, message: "Cannot read CV from DB."});
            }

            if (!dbCv) {
                return res.status(404).json({success: false, message: "Cannot find CV."});
            }
            dbCv.remove(function (err) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot delete CV from DB"});
                }
                return res.status(200).json({success: true, message: "CV deleted"});
            });
        });
    }

    function get_cvid_competences(req, res) {
        req.query.cv_id = req.params.id;
        return competencesController.get_root(req, res);
    }

    function post_cvid_competences(req, res) {
        req.body.cv_id = req.params.id;
        return competencesController.post_root(req, res);
    }

    return {
        get: root_get,
        post: root_post,

        get_cvid: get_cvid,
        put_cvid: put_cvid,
        delete_cvid: delete_cvid,

        get_cvid_competences: get_cvid_competences,
        post_cvid_competences: post_cvid_competences
    };
}

module.exports = moduleFunction;
