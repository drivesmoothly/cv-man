
"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    // optional dependencies
    var express = opts.express || require("express");
    var router = new express.Router();
    var Role = opts.Role || require("../../../models/project-role");
    var Project = opts.Project || require("../../../models/project");
    var json_role = opts.json_role || require("../json-formatters/json-role")(opts);

    router.route("/")
        .get(function (req, res) {
            var skip = req.headers.skip || 0;
            var limit = req.headers.limit || 100;
            limit = Math.min(100, limit);

            Role.find({}, "id name title project_id", {skip: skip, limit: limit}, function (err, dbRoles) {
                if (err) {
                    return req.status(500).json({success: false, message: "Cannot read project roles from DB."});
                }
                var roles = [];
                dbRoles.forEach(function (dbRole) {
                    roles.push(json_role.jsonRole(dbRole));
                });
                res.json(roles);
            });
        })

        .post(function (req, res) {
            // validate the necessary arguments
            if (!req.body.name || !req.body.project_id) {
                return res.status(400).json({success: false, message: "Role name or project id not specified."});
            }

            // make sure we have the project
            Project.findById(req.body.project_id, "id", function (err, project) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot get projects from DB."});
                }
                if (!project) {
                    return res.status(404).json({success: false, message: "Project not found"});
                }
                // make sure it's unique for the project
                Role.find({name: req.body.name, project_id: req.body.project_id}, "id", function (err, dbRoles) {
                    if (err) {
                        return res.status(500).json({success: false, message: "Cannot read project roles from DB."});
                    }
                    if (dbRoles.length) {
                        return res.status(409).json({success: false, message: "A role with the same name exists for the same project."});
                    }

                    var role = new Role({
                        name: req.body.name,
                        project_id: req.body.project_id,
                        title: req.body.title,
                        description: req.body.description
                    });
                    role.save(function (err) {
                        if (err) {
                            res.status(500).json({success: false, message: "Cannot save project role."});
                        }
                        res.json(json_role.jsonRole(role));
                    });
                });
            });
        });

    router.route("/:id")
        .get(function (req, res) {
            Role.findById(req.params.id, "id name title project_id description", function (err, dbRole) {
                if (err) {
                    if (err.kind === "ObjectId") {
                        return res.status(400).json({success: false, message: "Wrong ID format"});
                    }
                    return res.status(500).json({success: false, message: "Cannot read roles from DB."});
                }
                if (!dbRole) {
                    return res.status(404).json({success: false, message: "Role not found"});
                }
                res.json(json_role.jsonRole(dbRole));
            });
        })

        .put(function (req, res) {
            // Make sure we find the role
            Role.findById(req.params.id, "id name title project_id description", function (err, dbRole) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot read role from DB"});
                }
                if (!dbRole) {
                    return res.status(404).json({success: false, message: "Cannot find the role"});
                }
                // Update the accepted fields
                dbRole.name = req.body.name || dbRole.name;
                dbRole.title = req.body.title || dbRole.title;
                dbRole.description = req.body.description || dbRole.description;

                // save the update role
                dbRole.save(function (err) {
                    if (err) {
                        return res.status(500).json({success: false, message: "Cannot save role to db."});
                    }
                    return res.status(200).json(json_role.jsonRole(dbRole));
                });
            });
        })

        .delete(function (req, res) {
            Role.findById(req.params.id, function (err, dbRole) {
                if (err) {
                    return res.status(500).json({success: false, message: "Cannot read role from DB."});
                }
                if (!dbRole) {
                    return res.status(404).json({success: false, message: "Cannot find role."});
                }
                dbRole.remove(function (err) {
                    if (err) {
                        return res.status(500).json({success: false, message: "Cannot delete role."});
                    }
                    return res.status(200).json({success: true, message: "Role deleted."});
                });
            });
        });

    return router;
}

module.exports = moduleFunction;
