"use strict";

// using dependency injection via the "opt" variable
function moduleFunction(opt) {
    var opt = opt || {};

    // optional dependencies
    var express = opt.express || require("express");
    var User = opt.User || require("../../../../models/user");
    var bcrypt = opt.bcrypt || require("bcrypt-nodejs");
    var saltRounds = opt.saltRounds || 11;

    // variables
    var router = new express.Router();

    router.get("/", function (ignore, res) {
        bcrypt.genSalt(saltRounds, function (err, salt) {
            if (err) {
                throw err;
            }
            bcrypt.hash("johnny1", salt, null, function (err, hash) {
                if (err) {
                    throw err;
                }
                var marius = new User({
                    displayname: "Johnny Bravo",
                    email: "johnny.bravo@ro.neusoft.com",
                    auth: {
                        local: {password: hash},
                        ldap: {enabled: true}
                    }
                });
                marius.save(function (err) {
                    if (err) {
                        throw err;
                    }
                    console.log("User created successfully.");
                });
                res.json({success: true});
            });
        });
    });
    return router;
}

module.exports = moduleFunction;
