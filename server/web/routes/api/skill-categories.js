"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    var express = opts.express || require("express");
    var router = new express.Router();
    var controller = opts.skillCategoriesController || require("../../controllers/skill-categories-controller")(opts);

    router.route("/")
        .get(function (req, res) {
            return controller.root_get(req, res);
        })

        .post(function (req, res) {
            return controller.root_post(req, res);
        });

    router.route("/:id")
        .get(function (req, res) {
            return controller.get_id(req, res);
        })

        .put(function (req, res) {
            return controller.put_id(req, res);
        })

        .delete(function (req, res) {
            return controller.delete_id(req, res);
        });

    router.route("/:id/children")
        .get(function (req, res) {
            return controller.get_id_children(req, res);
        });

    return router;
}

module.exports = moduleFunction;
