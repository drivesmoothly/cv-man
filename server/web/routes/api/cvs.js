"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    var express = opts.express || require("express");
    var router = new express.Router();
    var cv_controller = opts.cv_controller || require("../../controllers/cvs-controller")(opts);

    router.route("/")
        .get(function (req, res) {
            return cv_controller.get(req, res);
        })

        .post(function (req, res) {
            return cv_controller.post(req, res);
        });

    router.route("/:id")
        .get(function (req, res) {
            return cv_controller.get_cvid(req, res);
        })

        .put(function (req, res) {
            return cv_controller.put_cvid(req, res);
        })

        .delete(function (req, res) {
            return cv_controller.delete_cvid(req, res);
        });

    router.route("/:id/competences")
        .get(function (req, res) {
            return cv_controller.get_cvid_competences(req, res);
        })

        .post(function (req, res) {
            return cv_controller.post_cvid_competences(req, res);
        });

    return router;
}

module.exports = moduleFunction;
