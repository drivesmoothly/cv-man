"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    var express = opts.express || require("express");
    var router = new express.Router();
    var competences_controller = opts.competences_controller || require("../../controllers/competences-controller")(opts);

    router.route("/")
        .get(function (req, res) {
            return competences_controller.get_root(req, res);
        })

        .post(function (req, res) {
            return competences_controller.post_root(req, res);
        });

    router.route("/:id")
        .get(function (req, res) {
            return competences_controller.get_compid(req, res);
        })

        .put(function (req, res) {
            return competences_controller.put_compid(req, res);
        })

        .delete(function (req, res) {
            return competences_controller.delete_compid(req, res);
        });

    return router;
}

module.exports = moduleFunction;
