"use strict";

var hasOwnPropertyNested = require("../../../utils/has-own-property-nested");

// function to compose a log message
function logMessage(email, id, message) {
    return "AD auth for " + email + "/" + id + ": " + message;
}

// using dependency injection via the "opt" variable
function moduleFunction(opt) {
    var opts = opt || {};

    // mandatory dependencies
    var ad = opts.ad;
    var superSecret = opts.superSecret;

    // optional dependencies
    var express = opts.express || require("express");
    var jwt = opts.jwt || require("jsonwebtoken");
    var User = opts.User || require("../../../models/user");

    // finds a local user
    function userExists(email, next) {
        User.findOne({email: email}, function (err, user) {
            if (err) {
                next(err, null);
            }
            next(null, user);
        });
    }

    // creates a local user to match the AD user, if one doesn't already exist
    function createLocalUser(localUser, adUser, next) {
        if (localUser && localUser.email === adUser.mail) {
            // we already have the local user
            next(null, localUser);
        } else {
            console.log(logMessage(adUser.mail, null, "User does not exist, creating user."));
            var newUser = new User({displayname: adUser.cn, email: adUser.mail,
                    auth: {ldap: {enabled: true}}});
            newUser.save(function (err) {
                if (err) {
                    next(err, null);
                }
                console.log(logMessage(newUser.email, newUser.id, "User created successfully."));
                next(null, newUser);
            });
        }
    }

    var router = new express.Router();
    var failResponse = {success: false, message: "Authentication failed."};

    router.post("/", function (req, res) {

        // check for the fields
        if (!req.body.email || !req.body.password) {
            return res.status(400).json({success: false, message: "email or password not provided."});
        }
        // find a local user and check if AD login is enabled
        userExists(req.body.email, function (err, localUser) {
            if (err) {
                return res.status(500).json({success: false, message: "Error connecting to DB."});
            }
            if (localUser && hasOwnPropertyNested(localUser, "auth", "ldap", "enabled") && !localUser.auth.ldap.enabled) {
                // login with LDAP is specifically disabled
                console.log(logMessage(req.body.email, null, "LDAP authentication is disabled"));
                res.json(failResponse);
            } else {
                // extract the user name from the email address
                var userName = req.body.email;
                var nameMatch = userName.match(/^([^@]*)@/);
                if (nameMatch) {
                    userName = nameMatch[1];
                }

                // authenticate on active directory
                ad.findUser(userName, function (err, adUser) {
                    if (err || !adUser) {
                        console.log(logMessage(userName, null, "User not found in AD."));
                        res.json(failResponse);
                    } else {
                        // the user was found, let's authenticate
                        ad.authenticate(adUser.dn, req.body.password, function (err, auth) {
                            if (err || !auth) {
                                console.log(logMessage(userName, null, "Password mismatch."));
                                res.json(failResponse);
                            } else {
                                // we are successfully authenticated
                                // create a local user if we don't already have one
                                createLocalUser(localUser, adUser, function (err, localUser) {
                                    if (err) {
                                        throw err;
                                    }
                                    var token = jwt.sign({user: localUser.id}, superSecret, {expiresIn: "24h"});
                                    res.json({success: true, message: "Enjoy!", token: token});
                                    console.log(logMessage(localUser.email, localUser.id, "Login successful."));
                                });
                            }
                        });
                    }
                });
            }
        });
    });

    return router;
}

module.exports = moduleFunction;
