
"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    // optional dependencies
    var express = opts.express || require("express");
    var router = new express.Router();
    var Project = opts.Project || require("../../../models/project");
    var Role = opts.Role || require("../../../models/project-role");
    var json_role = opts.json_role || require("../json-formatters/json-role")(opts);
    var json_project = opts.json_project || require("../json-formatters/json-project")(opts);

    router.route("/")
        .get(function (req, res) {
            var skip = req.headers.skip || 0;
            var limit = req.headers.limit || 100;
            limit = Math.min(100, limit);

            Project.find({}, "id name shortdescription fulldescription startdate enddate", {skip: skip, limit: limit}, function (err, dbProjects) {
                if (err) {
                    return req.status(500).json({success: false, message: "Error retrieving projects from DB."});
                }
                var projects = [];
                dbProjects.forEach(function (dbProject) {
                    projects.push(json_project.jsonProject(dbProject));
                });
                res.json(projects);
            });
        })

        .post(function (req, res) {
            // validate the necessary arguments
            // the only really necessary attribute is the project name
            if (!req.body.name) {
                return res.status(400).json({success: false, message: "Project name not specified."});
            }
            // make sure there isn't another project with the same name
            Project.find({name: req.body.name}, function (err, projects) {
                if (err) {
                    throw err;
                }
                if (projects.length) {
                    // project already exists
                    return res.status(409).json({success: false, message: "Project already exists"});
                }

                // create the new project
                var project = new Project({
                    name: req.body.name,
                    shortdescription: req.body.shortdescription,
                    fulldescription: req.body.fulldescription,
                    startdate: req.body.startdate,
                    enddate: req.body.enddate
                });
                project.save(function (err) {
                    if (err) {
                        return res.status(500).json({success: false, message: "Cannot save project."});
                    }
                    // return the newly created project
                    res.json(json_project.jsonProject(project));
                });
            });
        });

    router.route("/:project_id")
        .get(function (req, res) {
            Project.findById(req.params.project_id,
                    "id name shortdescription fulldescription startdate enddate",
                    function (err, dbProject) {
                if (err) {
                    if (err.kind === "ObjectId") {
                        return res.status(400).json({success: false, message: "Project ID in a wrong format."});
                    }
                    return res.status(500).json({success: false, message: "Cannot read project from DB."});
                }
                if (!dbProject) {
                    return res.status(404).json({success: false, message: "Cannot find project."});
                }
                res.json(json_project.jsonProject(dbProject));
            });
        })

        .put(function (req, res) {
            Project.findById(req.params.project_id,
                    "id name shortdescription fulldescription startdate enddate",
                    function (err, dbProject) {
                if (err) {
                    if (err.kind === "ObjectId") {
                        return res.status(400).json({success: false, message: "Project ID in a wrong format."});
                    }
                    return res.status(500).json({success: false, message: "Canot read project from DB."});
                }
                if (!dbProject) {
                    return res.status(404).json({success: false, message: "Cannot find project."});
                }

                // change the project
                dbProject.name = req.body.name || dbProject.name;
                dbProject.shortdescription = req.body.shortdescription || dbProject.shortdescription;
                dbProject.fulldescription = req.body.fulldescription || dbProject.fulldescription;
                dbProject.startdate = req.body.startdate || dbProject.startdate;
                dbProject.enddate = req.body.enddate || dbProject.enddate;

                // save project
                dbProject.save(function (err) {
                    if (err) {
                        return res.status(500).json({success: false, message: "Cannot save project."});
                    }
                    res.json(json_project.jsonProject(dbProject));
                });
            });
        })

        .delete(function (req, res) {
            Project.findById(req.params.project_id, "id", function (err, dbProject) {
                if (err) {
                    if (err.kind === "ObjectId") {
                        return res.status(400).json({success: false, message: "Project ID in a wrong format."});
                    }
                    return res.status(500).json({success: false, message: "Cannot read project from DB."});
                }
                if (!dbProject) {
                    return res.status(404).json({success: false, message: "Cannot find project."});
                }
                dbProject.remove(function (err) {
                    if (err) {
                        res.status(500).json({success: false, message: "Cannot delete project."});
                    }
                    res.json({success: true, message: "Project deleted"});
                });
            });
        });

    router.route("/:project_id/roles")
        .get(function (req, res) {
            Project.findById(req.params.project_id, "id", function (err, dbProject) {
                if (err) {
                    if (err.kind === "ObjectId") {
                        return res.status(400).json({success: false, message: "Project ID in a wrong format."});
                    }
                    return res.status(500).json({success: false, message: "Cannot read project from DB."});
                }
                if (!dbProject) {
                    return res.status(404).json({success: false, message: "Cannot find project."});
                }

                // Find the roles
                Role.find({project_id: req.params.project_id}, "id name title", function (err, dbRoles) {
                    if (err) {
                        return res.status(500).json({success: false, message: "Cannot read roles from DB."});
                    }
                    var roles = [];
                    dbRoles.forEach(function (dbRole) {
                        roles.push(json_role.jsonShortRole(dbRole));
                    });
                    res.json(roles);
                });
            });
        })

        .post(function (req, res) {
            Project.findById(req.params.project_id, "id", function (err, dbProject) {
                if (err) {
                    if (err.kind === "ObjectId") {
                        return res.status(400).json({success: false, message: "Invalid project id format."});
                    }
                    return res.status(500).json({success: false, message: "Cannot read project from DB."});
                }
                if (!dbProject) {
                    return res.status(404).json({success: false, message: "Cannot find project."});
                }
                if (!req.body.name) {
                    return res.status(400).json({success: false, message: "Role name is missing"});
                }

                // find roles with the same name in the same project
                Role.find({name: req.body.name, project_id: req.params.project_id}, "id", function (err, dbRoles) {
                    if (err) {
                        res.status(500).json({success: false, message: "Cannot read roles from DB."});
                    }
                    if (dbRoles.length) {
                        return res.status(409).json({
                            success: false,
                            message: "A role with that name already exists.",
                            links: {
                                role: opts.baseURL + "/api/roles/" + dbRoles[0].id
                            }
                        });
                    }

                    // create the new role
                    var role = new Role({
                        name: req.body.name,
                        project_id: req.params.project_id,
                        description: req.body.description,
                        title: req.body.title
                    });
                    role.save(function (err) {
                        if (err) {
                            res.status(500).json({success: false, message: "Cannot save role."});
                        }
                        res.json(json_role.jsonShortRole(role));
                    });
                });
            });
        });

    return router;
}

module.exports = moduleFunction;
