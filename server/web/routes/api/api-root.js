"use strict";

// using dependency injection via the "opt" variable
function moduleFunction(opt) {
    var opts = opt || {};

    // optional dependencies
    var express = opts.express || require("express");
    var router = new express.Router();
    var jwt = opts.jwt || require("jsonwebtoken");
    opts.jwt = jwt;

    var setup = opts.setup || require("./test/setup")(opts);
    var localSession = opts.localSession || require("./local-sessions")(opts);
    var ldapSession = opts.adSession || require("./ldap-sessions")(opts);
    var users = opts.users || require("./users")(opts);
    var projects = opts.projects || require("./projects")(opts);
    var projectRoles = opts.projectRoles || require("./project-roles")(opts);
    var cvs = opts.cvs || require("./cvs")(opts);
    var skillCategories = opts.skillCategories || require("./skill-categories")(opts);
    var skills = opts.skills || require("./skills")(opts);
    var competences = opts.competencesRoute || require("./competences")(opts);

    // Route to setup a user
    router.use("/test/setup", setup);

    // route to show a random message
    router.get("/", function (ignore, res) {
        res.json({message: "Welcome to the CV-MAN API!"});
    });
    router.use("/localsession", localSession);
    router.use("/ldapsession", ldapSession);

    // route middleware to verify the token
    router.use(function (req, res, next) {
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        if (token) {
            jwt.verify(token, opts.superSecret, function (err, decoded) {
                if (err) {
                    return res.json({success: false, message: "Failed to authenticate token"});
                }
                req.decoded = decoded;
                next();
            });
        } else {
            return res.status(403).json({success: false, message: "No token provided"});
        }
    });

    router.use("/users", users);
    router.use("/projects", projects);
    router.use("/projectroles", projectRoles);
    router.use("/cvs", cvs);
    router.use("/skillcategories", skillCategories);
    router.use("/skills", skills);
    router.use("/competences", competences);

    return router;
}

module.exports = moduleFunction;
