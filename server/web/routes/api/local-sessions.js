"use strict";

var hasPropertyNested = require("../../../utils/has-own-property-nested");

function composeMessageForUser(email, id, message) {
    return "Local auth for " + email + "/" + id + ": " + message;
}

// using dependency injection via the "opt" variable
function moduleFunction(opt) {
    var opts = opt || {};

    // mandatory dependencies
    var superSecret = opts.superSecret;

    // optional dependencies
    var express = opts.express || require("express");
    var User = opts.User || require("../../../models/user");
    var bcrypt = opts.bcrypt || require("bcrypt-nodejs");
    var jwt = opts.jwt || require("jsonwebtoken");

    // variables
    var router = new express.Router();
    var failResponse = {success: false, message: "Authentication failed."};

    router.post("/", function (req, res) {
        User.findOne({email: req.body.email}, function (err, user) {
            if (err) {
                // console.log("error requesting db")
                return res.status(500).json({success: false, message: "Cannot connect to DB."});
            }
            if (!user) {
                // console.log(composeMessageForUser(null, null, "User not found."));
                return res.status(401).json(failResponse);
            }
            // check if the user has local auth policy
            if (hasPropertyNested(user, "auth", "local", "enabled") && !user.auth.local.enabled) {
                // console.log(composeMessageForUser(user.email, user.id, "Local authentication not enabled."));
                res.json(failResponse);
            } else if (!hasPropertyNested(user, "auth", "local", "password")) {
                // console.log(composeMessageForUser(user.email, user.id, "Local password missing."));
                res.json(failResponse);
            } else {
                // check if the password matches
                bcrypt.compare(req.body.password, user.auth.local.password, function (err, match) {
                    if (err) {
                        throw err;
                    }
                    if (!match) {
                        // console.log(composeMessageForUser(user.email, user.id, "Password mismatch."));
                        res.status(401).json(failResponse);
                    } else {
                        var token = jwt.sign({user: user.id}, superSecret, {expiresIn: "24h"});
                        res.json({success: true, message: "Enjoy!", token: token});
                        // console.log(composeMessageForUser(user.email, user.id, "Login successful."));
                    }
                });
            }
        });
    });
    return router;
}

module.exports = moduleFunction;
