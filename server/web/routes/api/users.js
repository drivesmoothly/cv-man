
"use strict";

var hasPropertyNested = require("../../../utils/has-own-property-nested");

function moduleFunction(opt) {
    var opts = opt || {};

    // optional dependencies
    var express = opts.express || require("express");
    var router = new express.Router();
    var User = opts.User || require("../../../models/user");
    var Cv = opts.Cv || require("../../../models/cv");
    var bcrypt = opts.bcrypt || require("bcrypt-nodejs");
    var json_user = opts.json_user || require("../json-formatters/json-user")(opts);
    var cv_controller = opts.cv_controller || require("../../controllers/cvs-controller")(opts);

    function saveUser(user, res) {
        user.save(function (err) {
            if (err) {
                return res.status(500).json({success: false, message: "Cannot save user."});
            }
            return res.json(json_user.jsonUser(user));
        });
    }

    router.route("/")
        .get(function (req, res) {
            // get pagination parameters
            var skip = req.headers.skip || 0;
            var limit = req.headers.limit || 100;
            limit = Math.min(100, limit);

            User.find({}, "id displayname email", {skip: skip, limit: limit}, function (err, dbUsers) {
                if (err) {
                    res.status(500).json({message: "Error retrieving users from db"});
                } else {
                    var users = [];
                    dbUsers.forEach(function (dbUser) {
                        users.push(json_user.jsonUser(dbUser));
                    });
                    res.json(users);
                }
            });
        })

        .post(function (req, res) {
            // validate we have the necessary info
            if (!req.body.displayname || !req.body.email) {
                return res.status(400).json({success: false, message: "displayname or email missing."});
            }
            // see if there is already a user with that email
            User.find({email: req.body.email}, function (err, existingUser) {
                if (err) {
                    return res.status(500).json({success: false, message: "Error retrieving users."});
                }
                if (existingUser.length) {
                    return res.status(409).json({success: false, message: "User already exists."});
                }
                var user = new User({
                    displayname: req.body.displayname,
                    email: req.body.email
                });
                if (hasPropertyNested(req.body, "auth", "ldap", "enabled")) {
                    user.auth.ldap.enabled = req.body.auth.ldap.enabled;
                }
                if (hasPropertyNested(req.body, "auth", "local", "enabled")) {
                    user.auth.local.enabled = req.body.auth.local.enabled;
                }
                var needToSave = true;
                if (hasPropertyNested(req.body, "auth", "local", "password")) {
                    needToSave = false;
                    bcrypt.genSalt(opts.saltRounds, function (err, salt) {
                        if (err) {
                            return res.status(500).json({success: false, message: "Cannot hash password."});
                        }
                        bcrypt.hash(req.body.auth.local.password, salt, null, function (err, hash) {
                            if (err) {
                                return res.status(500).json({success: false, message: "Cannot hash password."});
                            }
                            user.auth.local.password = hash;
                            return saveUser(user, res);
                        });
                    });
                }
                if (needToSave) {
                    saveUser(user, res);
                }
            });
        });

    router.route("/:user_id")
        .get(function (req, res) {
            User.findById(req.params.user_id, "id displayname email", function (err, user) {
                if (err) {
                    if (err.kind === "ObjectId") {
                        return res.status(400).json({success: false, message: "User ID in a wrong format."});
                    }
                    return res.status(500).json({success: false, message: "Cannot access the db."});
                }
                if (!user) {
                    return res.status(404).json({success: false, message: "User not found."});
                }
                res.json(json_user.jsonUser(user));
            });
        })

        .put(function (req, res) {
            User.findById(req.params.user_id, function (err, user) {
                if (err) {
                    if (err.kind === "ObjectId") {
                        return res.status(400).json({success: false, message: "Invalid user id format."});
                    }
                    return res.status(500).json({success: false, message: "Could not access DB."});
                }
                if (!user) {
                    return res.status(404).json({success: false, message: "User not found."});
                }
                user.displayname = req.body.displayname || user.displayname;
                user.email = req.body.email || user.email;
                if (hasPropertyNested(req.body, "auth", "ldap", "enabled")) {
                    user.auth.ldap.enabled = req.body.auth.ldap.enabled;
                }
                if (hasPropertyNested(req.body, "auth", "local", "enabled")) {
                    user.auth.local.enabled = req.body.auth.local.enabled;
                }
                user.save(function (err) {
                    if (err) {
                        return res.status(500).json({success: false, message: "Could not update user."});
                    }
                    res.json(json_user.jsonUser(user));
                });
            });
        })

        .delete(function (req, res) {
            User.findById(req.params.user_id, function (err, user) {
                if (err) {
                    if (err.kind === "ObjectId") {
                        return res.status(400).json({success: false, message: "Invalid user id format."});
                    }
                    return res.status(500).json({success: false, message: "Cannot access DB."});
                }
                if (!user) {
                    return res.status(404).json({success: false, message: "Cannot find user."});
                }
                user.remove(function (err) {
                    if (err) {
                        return res.status(500).json({success: false, message: "Cannot remove user."});
                    }
                    // remove all the cvs for this user
                    Cv.remove({user_id: req.params.user_id}, function (err) {
                        if (err) {
                            console.log("Cannot remove the cvs for user", req.params.user_id);
                        }
                        res.json({success: true});
                    });
                });
            });
        });

    router.route("/:user_id/cvs")
        .get(function (req, res) {
            req.query.user_id = req.params.user_id;
            return cv_controller.get(req, res);
        });

    return router;
}

module.exports = moduleFunction;
