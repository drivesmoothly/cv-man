"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    function jsonSkill(dbSkill) {
        return {
            id: dbSkill.id,
            name: dbSkill.name,
            category_id: dbSkill.category_id,
            links: {
                self: opts.baseURL + "/api/skills/" + dbSkill.id,
                category: opts.baseURL + "/api/skillcategories/" + dbSkill.category_id
            }
        };
    }

    return {
        jsonSkill: jsonSkill
    };
}

module.exports = moduleFunction;
