"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    function jsonCv(dbCv) {
        return {
            id: dbCv.id,
            name: dbCv.name,
            user_id: dbCv.user_id,
            job_title: dbCv.job_title,
            links: {
                self: opts.baseURL + "/api/cvs/" + dbCv.id,
                user: opts.baseURL + "/api/users/" + dbCv.user_id,
                download: opts.baseURL + "/api/cvs/" + dbCv.id + "/download"
            }
        };
    }

    function jsonCompetences(dbCv, detailedLinks) {
        var competences = [];
        if (dbCv.competences) {
            dbCv.competences.forEach(function (competence) {
                var formatted = {
                    id: competence.id,
                    skill_id: competence.skill_id,
                    experience: {
                        months: competence.experience.months,
                        since: competence.experience.since,
                        percentage: competence.experience.percentage
                    },
                    links: {
                        self: opts.baseURL + "/api/cvs/" + dbCv.id + "/competences/" + competence.id,
                        skill: opts.baseURL + "/api/skills/" + competence.id
                    }
                };
                if (detailedLinks) {
                    formatted.links.user = opts.baseURL + "/api/users/" + dbCv.user_id;
                    formatted.links.cv = opts.baseURL + "/api/cvs/" + dbCv.id;
                }
                competences.push(formatted);
            });
        }

        return competences;
    }

    function jsonCompetence(dbCompetence, dbCv, detailedLinks) {
        var competence = {
            id: dbCompetence.id,
            skill_id: dbCompetence.skill_id,
            experience: {
                months: dbCompetence.experience.months,
                since: dbCompetence.experience.since,
                percentage: dbCompetence.experience.percentage
            },
            links: {
                self: opts.baseURL + "/api/cvs/" + dbCv.id + "/competences/" + dbCompetence.id,
                skill: opts.baseURL + "/api/skills/" + dbCompetence.skill_id
            }
        };
        if (detailedLinks) {
            competence.links.user = opts.baseURL + "/api/users/" + dbCv.user_id;
            competence.links.cv = opts.baseURL + "/api/cvs/" + dbCv.id;
        }

        return competence;
    }

    function jsonDetailedCv(dbCv) {
        var cv = {
            id: dbCv.id,
            name: dbCv.name,
            user_id: dbCv.user_id,
            job_title: dbCv.job_title,
            office_location: dbCv.office_location,
            links: {
                self: opts.baseURL + "/api/cvs/" + dbCv.id,
                user: opts.baseURL + "/api/users/" + dbCv.user_id,
                printable: opts.baseURL + "/api/cvs/" + dbCv.id + "/printable"
            }
        };

        cv.competences = jsonCompetences(dbCv, false);

        return cv;
    }

    return {
        jsonCv: jsonCv,
        jsonDetailedCv: jsonDetailedCv,
        jsonCompetences: jsonCompetences,
        jsonCompetence: jsonCompetence
    };
}

module.exports = moduleFunction;
