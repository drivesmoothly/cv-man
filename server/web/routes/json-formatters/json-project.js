"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    function jsonProject(dbProject) {
        return {
            id: dbProject.id,
            name: dbProject.name,
            shortdescription: dbProject.shortdescription,
            fulldescription: dbProject.fulldescription,
            startdate: dbProject.startdate,
            enddate: dbProject.enddate,
            links: {
                self: opts.baseURL + "/api/projects/" + dbProject.id,
                roles: opts.baseURL + "/api/projects/" + dbProject.id + "/roles"
            }
        };
    }

    return {
        jsonProject: jsonProject
    };
}

module.exports = moduleFunction;
