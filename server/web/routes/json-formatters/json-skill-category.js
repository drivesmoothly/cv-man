"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    function jsonSkillCategory(dbCategory) {
        return {
            id: dbCategory.id,
            name: dbCategory.name,
            path: dbCategory.path,
            links: {
                self: opts.baseURL + "/api/skillcategories/" + dbCategory.id,
                children: opts.baseURL + "/api/skillcategories/" + dbCategory.id + "/children"
            }
        };
    }

    return {
        jsonSkillCategory: jsonSkillCategory
    };
}

module.exports = moduleFunction;
