"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    function jsonUser(dbUser) {
        return {
            id: dbUser.id,
            displayname: dbUser.displayname,
            email: dbUser.email,
            links: {
                self: opts.baseURL + "/api/users/" + dbUser.id,
                cvs: opts.baseURL + "/api/users/" + dbUser.id + "/cvs"
            }
        };
    }

    return {
        jsonUser: jsonUser
    };
}

module.exports = moduleFunction;
