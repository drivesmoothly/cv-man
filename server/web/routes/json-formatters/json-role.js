"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    function jsonShortRole(dbRole) {
        return {
            id: dbRole.id,
            name: dbRole.name,
            title: dbRole.title,
            links: {
                self: opts.baseURL + "/api/projectroles/" + dbRole.id,
                project: opts.baseURL + "/api/projects/" + dbRole.project_id
            }
        };
    }

    function jsonRole(dbRole) {
        return {
            id: dbRole.id,
            name: dbRole.name,
            title: dbRole.title,
            project_id: dbRole.project_id,
            description: dbRole.description,
            links: {
                self: opts.baseURL + "/api/projectroles/" + dbRole.id,
                project: opts.baseURL + "/api/projects/" + dbRole.project_id
            }
        };
    }

    return {
        jsonShortRole: jsonShortRole,
        jsonRole: jsonRole
    };
}

module.exports = moduleFunction;
