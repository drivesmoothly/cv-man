"use strict";

var Cv = require("../models/cv");

function createCvsFixture(name, user_id, title, skill_id, next) {

    var cv = new Cv({
        name: name || "Web Developer",
        user_id: user_id,
        job_title: title || "Web Developer",
        office_location: "Cluj Napoca",
        competences: [
            {
                skill_id: skill_id,
                experience: {
                    percentage: 100
                }
            }
        ]
    });

    cv.save(function (err) {
        if (err) {
            throw err;
        }
        next(null, cv);
    });
}

module.exports = createCvsFixture;
