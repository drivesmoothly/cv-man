
"use strict";

var mongoose = require("mongoose");

function clearDb(next) {

    mongoose.connection.dropDatabase(function (err) {
        if (err) {
            console.log(err);
            throw err;
        }
        next(null);
    });
}

module.exports = clearDb;
