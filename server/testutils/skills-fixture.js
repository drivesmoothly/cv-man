"use strict";

var Skill = require("../models/skill");

function createSkillFixture(name, category_id, next) {
    var category = new Skill({
        name: name || "JavaScript",
        category_id: category_id
    });

    category.save(function (err) {
        if (err) {
            throw err;
        }
        next(err, category);
    });
}

module.exports = createSkillFixture;
