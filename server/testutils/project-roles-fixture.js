
"use strict";

var Role = require("../models/project-role");

function createProjRolesFixture(projectId, next) {

    // create a sample project role
    var role = new Role({
        name: "Developer",
        title: "Web Developer",
        project_id: projectId,
        description: "Somebody burning hours for it"
    });

    role.save(function (err) {
        if (err) {
            throw err;
        }
        next(null, role);
    });
}

module.exports = createProjRolesFixture;
