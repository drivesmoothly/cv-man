"use strict";

var Competence = require("../models/competence");

function createCompetencesFixture(cv_id, skill_id, experience, next) {

    var competence = new Competence({
        cv_id: cv_id,
        skill_id: skill_id,
        experience: experience || {
            percentage: 100
        }
    });

    competence.save(function (err) {
        if (err) {
            throw err;
        }
        next(null, competence);
    });
}

module.exports = createCompetencesFixture;
