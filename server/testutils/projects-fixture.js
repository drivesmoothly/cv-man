
"use strict";

var Project = require("../models/project");

function createProjectsFixture(name, shortdesc, fulldesc, startdate, enddate, next) {

    // create a sample project
    var project = new Project({
        name: name || "Project1",
        shortdescription: shortdesc || "A sample project",
        fulldescription: fulldesc || "A sample project made up just to test the code",
        startdate: startdate || new Date(2011, 0, 10),
        enddate: enddate || new Date(2015, 11, 12)
    });

    project.save(function (err) {
        if (err) {
            throw err;
        }
        next(null, project);
    });
}

module.exports = createProjectsFixture;
