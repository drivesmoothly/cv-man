
"use strict";

var User = require("../models/user");

function createUsersFixture(displayname, email, localpass, next) {

    // create a sample user
    var user = new User({
        displayname: displayname || "Johnny Bravo",
        email: email || "johnny.bravo@ro.neusoft.com",
        auth: {local: {password: localpass || "$2a$10$hrkuuTN2TttMiTVj80ByPuLCm96Dqz4sYL0rwBXxhvZBL0dWHMkjW"}} //johnny1
    });

    user.save(function (err) {
        if (err) {
            throw err;
        }
        next(null, user);
    });
}

module.exports = createUsersFixture;
