"use strict";

var SkillCategory = require("../models/skill-category");

function createSkillCategoryFixture(name, path, next) {
    var category = new SkillCategory({
        name: name || "Technical",
        path: path || ""
    });

    category.save(function (err) {
        if (err) {
            throw err;
        }
        next(err, category);
    });
}

module.exports = createSkillCategoryFixture;
