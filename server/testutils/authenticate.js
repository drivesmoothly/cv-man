
"use strict";

var chai = require("chai");
var chaiHttp = require("chai-http");
chai.use(chaiHttp);
var server = require("../app");

function authenticate(aEmail, aPass, next) {
    var email = aEmail || "johnny.bravo@ro.neusoft.com";
    var pass = aPass || "johnny1";
    // login to get token
    chai.request(server)
        .post("/api/localsession")
        .set("content-type", "application/x-www-form-urlencoded")
        .send({email: email, password: pass})
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            var result = JSON.parse(res.text);
            next(null, result.token);
        });

}

module.exports = authenticate;
