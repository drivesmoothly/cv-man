"use strict";

function moduleFunction(opt) {
    var opts = opt || {};

    var chai = opts.chai || require("chai");
    var chaiHttp = opts.chaiHttp || require("chai-http");
    if (!opts.chai) {
        chai.use(chaiHttp);
    }

    var server = opts.server || require("../../app");

    function verify_get(path) {
        it("returns 403", function (done) {
            chai.request(server)
                .get(path)
                .end(function (err, res) {
                    res.should.have.status(403);
                    res.body.success.should.eql(false);
                    res.body.message.should.exist;
                    done();
                });
        });
    }

    function verify_post(path, data) {
        it("returns 403", function (done) {
            chai.request(server)
                .post(path)
                .send(data)
                .end(function (err, res) {
                    res.should.have.status(403);
                    res.body.success.should.eql(false);
                    res.body.message.should.exist;
                    done();
                });
        });
    }

    return {
        verify_get: verify_get,
        verify_post: verify_post
    };
}

module.exports = moduleFunction;
