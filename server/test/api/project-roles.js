"use strict";

process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");
var should = chai.should();
var server = require("../../app");

var createUsersFixture = require("../../testutils/users-fixture");
var cleanDb = require("../../testutils/clean-db");
var authenticate = require("../../testutils/authenticate");
var createProjRolesFixture = require("../../testutils/project-roles-fixture");
var createProjectsFixture = require("../../testutils/projects-fixture");

chai.use(chaiHttp);

describe("/api/projectroles", function () {

    describe("No token", function () {

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/projectroles")
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("POST", function () {
            var project = null;

            beforeEach(function (done) {
                cleanDb(function (err) {
                    createProjectsFixture(null, null, null, null, null, function (err, dbProject) {
                        project = dbProject;
                        done();
                    });
                });
            });

            it("returns 403", function (done) {
                chai.request(server)
                    .post("/api/projectroles")
                    .send({
                        name: "dev1",
                        title: "Web Developer",
                        description: "my description",
                        project_id: project.id
                    })
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var token = "";

        beforeEach(function (done) {
            cleanDb(function (err) {
                createUsersFixture(null, null, null, function (err, dbUser) {
                    authenticate(null, null, function (err, authToken) {
                        token = authToken;
                        done();
                    });
                });
            });
        });

        describe("GET", function () {

            describe("Empty list", function () {
                it("returns an empty list", function (done) {
                    chai.request(server)
                        .get("/api/projectroles")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(0);
                            done();
                        });
                });
            });

            describe("A sample role", function () {

                beforeEach(function (done) {
                    createProjRolesFixture(null, function (err, dbProjectRole) {
                        done();
                    });
                });

                it("returns a list with one item", function (done) {
                    chai.request(server)
                        .get("/api/projectroles")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(1);
                            res.body[0].name.should.eql("Developer");
                            done();
                        });
                });
            });

        });

        describe("POST", function () {

            describe("Role name exists", function () {

                var project = null;

                beforeEach(function (done) {
                    createProjectsFixture(null, null, null, null, null, function (err, dbProject) {
                        project = dbProject;
                        done();
                    });
                });

                describe("On the same project", function () {

                    var role = null;

                    beforeEach(function (done) {
                        createProjRolesFixture(project.id, function (err, dbRole) {
                            role = dbRole;
                            done();
                        });
                    });

                    it("returns 409", function (done) {
                        chai.request(server)
                            .post("/api/projectroles")
                            .set("x-access-token", token)
                            .send({
                                name: role.name,
                                project_id: project.id
                            })
                            .end(function (err, res) {
                                res.should.have.status(409);
                                res.body.success.should.eql(false);
                                res.body.message.should.exist;
                                done();
                            });
                    });
                });

                describe("On a different project", function () {

                    var role = null;
                    var newProjectId = null;

                    beforeEach(function (done) {
                        createProjRolesFixture(null, function (err, dbRole) {
                            role = dbRole;
                            createProjectsFixture("Project2", null, null, null, null, function (err, dbProject) {
                                newProjectId = dbProject.id;
                                done();
                            });
                        });
                    });

                    it("creates a role", function (done) {
                        chai.request(server)
                            .post("/api/projectroles")
                            .set("x-access-token", token)
                            .send({
                                name: role.name,
                                project_id: newProjectId
                            })
                            .end(function (err, res) {
                                res.should.have.status(200);
                                res.body.id.should.exist;
                                res.body.name.should.eql(role.name);
                                res.body.project_id.should.eql(newProjectId);
                                done();
                            });
                    });
                });
            });

            describe("Project doesn't exist", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .post("/api/projectroles")
                        .set("x-access-token", token)
                        .send({
                            name: "Web Developer 1",
                            project_id: "123456789012345678901234",
                            title: "Web Developer"
                        })
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });
    });
});

describe("/api/projectroles/:id", function () {

    var project = null;
    var role = null;

    beforeEach(function (done) {
        // make sure we have a project and a project role
        cleanDb(function () {
            createProjectsFixture(null, null, null, null, null, function (err, dbProject) {
                createProjRolesFixture(dbProject.id, function (err, dbRole) {
                    project = dbProject;
                    role = dbRole;
                    done();
                });
            });
        });
    });

    describe("No token", function () {

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/projectroles/" + role.id)
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("PUT", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .put("/api/projectroles/" + role.id)
                    .send({title: "The best project"})
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("DELETE", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .delete("/api/projectroles/" + role.id)
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var user = null;
        var token = null;

        beforeEach(function (done) {
            createUsersFixture(null, null, null, function (err, dbUser) {
                authenticate(null, null, function (err, authToken) {
                    user = dbUser;
                    token = authToken;
                    done();
                });
            });
        });

        describe("GET", function () {

            describe("Retrieves a role", function () {
                it("returns the role", function (done) {
                    chai.request(server)
                        .get("/api/projectroles/" + role.id)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.id.should.exist;
                            res.body.name.should.eql("Developer");
                            res.body.project_id.should.eql(project.id);
                            res.body.title.should.eql("Web Developer");
                            res.body.description.should.eql("Somebody burning hours for it");
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .get("/api/projectroles/12345")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .get("/api/projectroles/123456789012345678901234")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });

        describe("PUT", function () {

            describe("Updates a role", function () {
                it("returns 200", function (done) {
                    chai.request(server)
                        .put("/api/projectroles/" + role.id)
                        .set("x-access-token", token)
                        .send({title: "A funny new title"})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.id.should.eql(role.id);
                            res.body.title.should.eql("A funny new title");
                            done();
                        });
                });
            });

            describe("Update an inexisting role", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .put("/api/projectroles/123456789012345678901234")
                        .set("x-access-token", token)
                        .send({title: "A funny new title"})
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Project ID cannot be updated", function () {
                it("does not change the project_id", function (done) {
                    chai.request(server)
                        .put("/api/projectroles/" + role.id)
                        .set("x-access-token", token)
                        .send({project_id: "123456789012345678901234"})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.project_id.should.eql(role.project_id.toString());
                            done();
                        });
                });
            });
        });

        describe("DELETE", function () {

            describe("Delete a role", function () {
                it("returns 200", function (done) {
                    chai.request(server)
                        .delete("/api/projectroles/" + role.id)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.success.should.eql(true);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Delete a not existing role", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .delete("/api/projectroles/123456789012345678901234")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });
    });
});
