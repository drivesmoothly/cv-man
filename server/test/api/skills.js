"use strict";

process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");
chai.use(chaiHttp);

var should = chai.should();
var server = require("../../app");

var cleanDb = require("../../testutils/clean-db");
var createUsersFixture = require("../../testutils/users-fixture");
var authenticate = require("../../testutils/authenticate");
var skillCategoryFixture = require("../../testutils/skill-categories-fixture");
var skillFixture = require("../../testutils/skills-fixture");

describe("/api/skills", function () {

    describe("No token", function () {

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/skills")
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("POST", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .post("/api/skills")
                    .send({name: "new skill"})
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var user = null;
        var token = null;

        beforeEach(function (done) {
            cleanDb(function (err) {
                createUsersFixture(null, null, null, function (err, dbUser) {
                    user = dbUser;
                    authenticate(null, null, function (err, aToken) {
                        token = aToken;
                        done();
                    });
                });
            });
        });

        describe("GET", function () {

            describe("An empty list", function () {
                it("returns an empty list", function (done) {
                    chai.request(server)
                        .get("/api/skills")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(0);
                            done();
                        });
                });
            });

            describe("A sample skill", function () {

                var category = null;
                var skill = null;

                beforeEach(function (done) {
                    skillCategoryFixture(null, null, function (err, dbCategory) {
                        category = dbCategory;
                        skillFixture(null, dbCategory, function (err, dbSkill) {
                            skill = dbSkill;
                            done();
                        });
                    });
                });

                it("returns a list with one item", function (done) {
                    chai.request(server)
                        .get("/api/skills")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(1);
                            res.body[0].name.should.eql("JavaScript");
                            done();
                        });
                });
            });
        });

        describe("POST", function () {

            describe("Create a new skill", function () {
                it("creates a skill", function (done) {
                    chai.request(server)
                        .post("/api/skills")
                        .set("x-access-token", token)
                        .send({name: "c++"})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.name.should.eql("c++");
                            done();
                        });
                });
            });

            describe("Cannot create skill w/o name", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .post("/api/skills")
                        .set("x-access-token", token)
                        .send({category_id: null})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Cannot create a skill with a not existing category", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .post("/api/skills")
                        .set("x-access-token", token)
                        .send({name: "c++", category_id: "123456789012345678901234"})
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Cannot create a skill with invalid category ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .post("/api/skills")
                        .set("x-access-token", token)
                        .send({name: "c++", category_id: "12345"})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });
    });
});

describe("/api/skills/:id", function () {

    var skill = null;
    var category = null;

    beforeEach(function (done) {
        cleanDb(function (err) {
            skillFixture(null, null, function (err, dbSkill) {
                skill = dbSkill;
                skillCategoryFixture("technical", null, function (err, dbCategory) {
                    category = dbCategory;
                    done();
                });
            });
        });
    });

    describe("No token", function () {

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/skills-/" + skill.id)
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("PUT", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .put("/api/skills/" + skill.id)
                    .send({name: "new name"})
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("DELETE", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .delete("/api/skills/" + skill.id)
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var user = null;
        var token = null;

        beforeEach(function (done) {
            createUsersFixture(null, null, null, function (err, dbUser) {
                user = dbUser;
                authenticate(null, null, function (err, aToken) {
                    token = aToken;
                    done();
                });
            });
        });

        describe("GET", function () {

            describe("Retrieve a skill", function () {
                it("returns a skill ", function (done) {
                    chai.request(server)
                        .get("/api/skills/" + skill.id)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.name.should.eql("JavaScript");
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .get("/api/skills/12345")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .get("/api/skills/123456789012345678901234")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });

        describe("PUT", function () {

            describe("Update a skill", function () {
                it("updates the skill", function (done) {
                    chai.request(server)
                        .put("/api/skills/" + skill.id)
                        .set("x-access-token", token)
                        .send({name: "updated skill", category_id: category.id})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.name.should.eql("updated skill");
                            res.body.category_id.should.eql(category.id);
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .put("/api/skills/12345")
                        .set("x-access-token", token)
                        .send({name: "updated skill", category_id: category.id})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID ", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .put("/api/skills/123456789012345678901234")
                        .set("x-access-token", token)
                        .send({name: "updated skill", category_id: category.id})
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });

        describe("DELETE", function () {

            describe("Deletes a skill", function () {
                it("deletes a skill", function (done) {
                    chai.request(server)
                        .delete("/api/skills/" + skill.id)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.success.should.eql(true);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .delete("/api/skills/12345")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .delete("/api/skills/123456789012345678901234")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });
    });
});
