"use strict";

process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");
chai.use(chaiHttp);

var should = chai.should();
var server = require("../../app");

var cleanDb = require("../../testutils/clean-db");
var usersFixture = require("../../testutils/users-fixture");
var authenticate = require("../../testutils/authenticate");
var cvsFixture = require("../../testutils/cvs-fixture");
var skillCategoriesFixture = require("../../testutils/skill-categories-fixture");
var skillsFixture = require("../../testutils/skills-fixture");
var competenceFixture = require("../../testutils/competences-fixture");

describe("/api/competences", function () {

    var category = null;
    var skill = null;
    var user = null;
    var cv = null;
    var competence = null;

    beforeEach(function (done) {
        cleanDb(function (err) {
            skillCategoriesFixture(null, null, function (err, dbCategory) {
                category = dbCategory;
                skillsFixture(null, dbCategory.id, function (err, dbSkill) {
                    skill = dbSkill;
                    usersFixture(null, null, null, function (err, dbUser) {
                        user = dbUser;
                        cvsFixture(null, user.id, null, null, function (err, dbCv) {
                            cv = dbCv;
                            competenceFixture(cv.id, dbSkill.id, null, function (err, dbCompetence) {
                                competence = dbCompetence;
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

    describe("No token", function () {

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/competences")
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("POST", function () {

            var newSkill = null;

            beforeEach(function (done) {
                skillsFixture(null, category.id, function (err, dbNewSkill) {
                    newSkill = dbNewSkill;
                    done();
                });
            });

            it("returns 403", function (done) {
                chai.request(server)
                    .post("/api/competences")
                    .send({cv_id: cv.id, skill_id: skill.id, experience: {months: 24}})
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var token = null;

        beforeEach(function (done) {
            authenticate(null, null, function (err, aToken) {
                token = aToken;
                done();
            });
        });

        describe("GET", function () {

            describe("A single competence", function () {
                it("returns a competence", function (done) {
                    chai.request(server)
                        .get("/api/competences")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(1);
                            res.body[0].cv_id.should.eql(cv.id);
                            res.body[0].skill_id.should.eql(skill.id);
                            res.body[0].experience.percentage.should.eql(100);
                            done();
                        });
                });
            });

            describe("Multiple competences", function () {

                var newSkill = null;
                var newCompetence = null;

                beforeEach(function (done) {
                    skillsFixture(null, category.id, function (err, dbNewSkill) {
                        newSkill = dbNewSkill;
                        competenceFixture(cv.id, newSkill.id, {percentage: 50}, function (err, dbNewCompetence) {
                            newCompetence = dbNewCompetence;
                            done();
                        });
                    });
                });


                describe("Get all of them", function () {
                    it("retrieves 2 competences", function (done) {
                        chai.request(server)
                            .get("/api/competences")
                            .set("x-access-token", token)
                            .end(function (err, res) {
                                res.should.have.status(200);
                                res.body.should.be.a("array");
                                res.body.length.should.eql(2);
                                done();
                            });
                    });
                });

                describe("Filter only one", function () {
                    it("retrieves only one competence", function (done) {
                        chai.request(server)
                            .get("/api/competences?skill_id=" + newSkill.id)
                            .set("x-access-token", token)
                            .end(function (err, res) {
                                res.should.have.status(200);
                                res.body.should.be.a("array");
                                res.body[0].skill_id.should.eql(newSkill.id);
                                done();
                            });
                    });
                });
            });
        });

        describe("POST", function () {

            var newSkill = null;

            beforeEach(function (done) {
                skillsFixture(null, category.id, function (err, dbNewSkill) {
                    newSkill = dbNewSkill;
                    done();
                });
            });

            describe("Create a new competence", function () {
                it("creates a new competence", function (done) {
                    chai.request(server)
                        .post("/api/competences")
                        .set("x-access-token", token)
                        .send({cv_id: cv.id, skill_id: newSkill.id, experience: {months: 12}})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.cv_id.should.eql(cv.id);
                            res.body.skill_id.should.eql(newSkill.id);
                            res.body.experience.months.should.eql(12);
                            done();
                        });
                });
            });

            describe("Cannot create 2 competences for the same skill", function () {
                it("returns 409", function (done) {
                    chai.request(server)
                        .post("/api/competences")
                        .set("x-access-token", token)
                        .send({cv_id: cv.id, skill_id: skill.id, experience: {months: 12}})
                        .end(function (err, res) {
                            res.should.have.status(409);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("CV ID is mandatory", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .post("/api/competences")
                        .set("x-access-token", token)
                        .send({skill_id: newSkill.id, experience: {months: 12}})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Skill ID is mandatory", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .post("/api/competences")
                        .set("x-access-token", token)
                        .send({cv_id: cv.id, experience: {months: 12}})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });
    });
});

describe("/api/competences/:id", function () {

    var category = null;
    var skill = null;
    var user = null;
    var cv = null;
    var competence = null;

    beforeEach(function (done) {
        cleanDb(function (err) {
            skillCategoriesFixture(null, null, function (err, dbCategory) {
                category = dbCategory;
                skillsFixture(null, dbCategory.id, function (err, dbSkill) {
                    skill = dbSkill;
                    usersFixture(null, null, null, function (err, dbUser) {
                        user = dbUser;
                        cvsFixture(null, user.id, null, null, function (err, dbCv) {
                            cv = dbCv;
                            competenceFixture(cv.id, dbSkill.id, null, function (err, dbCompetence) {
                                competence = dbCompetence;
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

    describe("No token", function () {

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/competences/" + competence.id)
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("PUT", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .put("/api/competences/" + competence.id)
                    .send({experience: {percentage: 90}})
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("DELETE", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .delete("/api/competences/" + competence.id)
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var token = null;

        beforeEach(function (done) {
            authenticate(null, null, function (err, aToken) {
                token = aToken;
                done();
            });
        });

        describe("GET", function () {

            describe("Retrieve a competence", function () {
                it("returns a competence", function (done) {
                    chai.request(server)
                        .get("/api/competences/" + competence.id)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.cv_id.should.exist;
                            res.body.skill_id.should.exist;
                            res.body.experience.percentage.should.eql(100);
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .get("/api/competences/12345")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .get("/api/competences/123456789012345678901234")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });

        describe("PUT", function () {

            describe("Change a competence", function () {
                it("changes the competence", function (done) {
                    chai.request(server)
                        .put("/api/competences/" + competence.id)
                        .set("x-access-token", token)
                        .send({experience: {percentage: 90}})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.experience.percentage.should.eql(90);
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .put("/api/competences/12345")
                        .set("x-access-token", token)
                        .send({experience: {percentage: 90}})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .put("/api/competences/123456789012345678901234")
                        .set("x-access-token", token)
                        .send({experience: {percentage: 90}})
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });

        describe("DELETE", function () {

            describe("Deletes a competence", function () {
                it("returns 200", function (done) {
                    chai.request(server)
                        .delete("/api/competences/" + competence.id)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.success.should.eql(true);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .delete("/api/competences/12345")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .delete("/api/competences/123456789012345678901234")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });
    });
});
