"use strict";

process.env.NODE_ENV = "test";

var chai                    = require("chai");
var chaiHttp                = require("chai-http");
var should                  = chai.should();
var server                  = require("../../app");

var createProjectsFixture   = require("../../testutils/projects-fixture");
var createUsersFixture      = require("../../testutils/users-fixture");
var clearDb                 = require("../../testutils/clean-db");
var authenticate            = require("../../testutils/authenticate");
var createProjRolesFixture  = require("../../testutils/project-roles-fixture");

chai.use(chaiHttp);


describe("/api/projects", function () {

    describe("No token", function () {

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/projects")
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("POST", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .post("/api/projects")
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {
        var token = "";

        beforeEach(function (done) {
            clearDb(function (err) {
                createUsersFixture(null, null, null, function (err, user) {
                    authenticate(null, null, function (err, aToken) {
                        token = aToken;
                        done();
                    });
                });
            });
        });

        describe("GET", function () {

            describe("Empty list", function () {
                it("returns an empty projects list", function (done) {
                    chai.request(server)
                        .get("/api/projects")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(0);
                            done();
                        });
                });
            });

            describe("A sample project", function () {

                beforeEach(function (done) {
                    createProjectsFixture(null, null, null, null, null, function (err) {
                        done();
                    });
                });

                it("returns a projects list with the sample project", function (done) {
                    chai.request(server)
                        .get("/api/projects")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            if (err) {
                                throw err;
                            }
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(1);
                            res.body[0].name.should.eql("Project1");
                            res.body[0].shortdescription.should.eql("A sample project");
                            done();
                        });
                });
            });
        });

        describe("POST", function () {

            describe("Project exists", function () {

                var project = null;

                beforeEach(function (done) {
                    createProjectsFixture(null, null, null, null, null, function (err, dbProject) {
                        project = dbProject;
                        done();
                    });
                });

                it("returns 409", function (done) {
                    chai.request(server)
                        .post("/api/projects")
                        .set("x-access-token", token)
                        .send({name: project.name})
                        .end(function (err, res) {
                            res.should.have.status(409);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("No name for project", function () {

                it("returns 400", function (done) {
                    chai.request(server)
                        .post("/api/projects")
                        .set("x-access-token", token)
                        .send({shortdescription: "Another project for testing the API"})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Create a project", function () {

                it("creates a new project", function (done) {
                    chai.request(server)
                        .post("/api/projects")
                        .set("x-access-token", token)
                        .send({name: "Project 2", shortdescription: "Another project for testing the API"})
                        .end(function (err, res) {
                            if (err) {
                                throw err;
                            }
                            res.should.have.status(200);
                            res.body.id.should.exist;
                            res.body.name.should.eql("Project 2");
                            res.body.shortdescription.should.eql("Another project for testing the API");
                            done();
                        });
                });
            });
        });
    });
});

describe("/api/projects/:id", function () {

    describe("No token", function () {

        var projectId = "";

        beforeEach(function (done) {
            clearDb(function (err) {
                createProjectsFixture(null, null, null, null, null, function (err, project) {
                    projectId = project.id;
                    done();
                });
            });
        });

        describe("GET", function () {
            it ("returns 403", function (done) {
                chai.request(server)
                    .get("/api/projects/" + projectId)
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("PUT", function () {
            it ("returns 403", function (done) {
                chai.request(server)
                    .put("/api/projects/" + projectId)
                    .send({name: "new name"})
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("DELETE", function () {
            it ("returns 403", function (done) {
                chai.request(server)
                    .delete("/api/projects/" + projectId)
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {
        var token = "";
        var projectId = "";

        beforeEach(function (done) {
            clearDb(function (err) {
                createUsersFixture(null, null, null, function (err, user) {
                    authenticate(null, null, function (err, aToken, aUserId) {
                        createProjectsFixture(null, null, null, null, null, function (err, dbProject) {
                            token = aToken;
                            projectId = dbProject.id;
                            done();
                        });
                    });
                });
            });
        });

        describe("GET", function () {

            describe("Wrong id format", function () {
                it ("returns 400", function (done) {
                    chai.request(server)
                        .get("/api/projects/12345")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.equal(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong id", function () {
                it ("returns 404", function (done) {
                    chai.request(server)
                        .get("/api/projects/123456789012345678901234")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Get project details", function () {
                it ("returns the project details", function (done) {
                    chai.request(server)
                        .get("/api/projects/" + projectId)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.name.should.eql("Project1");
                            res.body.id.should.eql(projectId);
                            res.body.shortdescription.should.exist;
                            done();
                        });
                });
            });
        });

        describe("PUT", function () {

            describe("Wrong id format", function () {
                it ("returns 400", function (done) {
                    chai.request(server)
                        .put("/api/projects/12345")
                        .set("x-access-token", token)
                        .send({name: "a new name"})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong id", function () {
                it ("returns 404", function (done) {
                    chai.request(server)
                        .put("/api/projects/123456789012345678901234")
                        .set("x-access-token", token)
                        .send({name: "a new name"})
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Change a project", function () {
                it ("changes the project", function (done) {
                    chai.request(server)
                        .put("/api/projects/" + projectId)
                        .set("x-access-token", token)
                        .send({
                            name: "a new name",
                            shortdescription: "abc",
                            fulldescription: "def",
                            startdate: new Date(2018, 4, 1),
                            enddate: new Date(2019, 3, 30)
                        })
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.name.should.eql("a new name");
                            res.body.id.should.eql(projectId);
                            res.body.shortdescription.should.eql("abc");
                            res.body.fulldescription.should.eql("def");
                            res.body.startdate.should.eql(new Date(2018, 4, 1).toISOString());
                            res.body.enddate.should.eql(new Date(2019, 3, 30).toISOString());
                            done();
                        });
                });
            });

        });

        describe("DELETE", function () {

            describe("Wrong id format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .delete("/api/projects/12345")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong id", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .delete("/api/projects/123456789012345678901234")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Delete a project", function () {
                it("deletes the project", function (done) {
                    chai.request(server)
                        .delete("/api/projects/" + projectId)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.success.should.eql(true);
                            res.body.message.should.exist;

                            // make sure the project is deleted
                            chai.request(server)
                                .get("/api/projects/" + projectId)
                                .set("x-access-token", token)
                                .end(function (err, res) {
                                    res.should.have.status(404);
                                    done();
                                });
                        });
                });
            });
        });
    });
});

describe("/api/projects/:id/roles", function() {

    describe("No token", function () {

        var projectId = "";

        beforeEach(function (done) {
            clearDb(function (err) {
                createProjectsFixture(null, null, null, null, null, function (err, project) {
                    projectId = project.id;
                    done();
                });
            });
        });

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/projects/" + projectId + "/roles")
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("POST", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .post("/api/projects/" + projectId + "/roles")
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var token = "";
        var projectId = "";

        beforeEach(function (done) {
            clearDb(function (err) {
                createUsersFixture(null, null, null, function (err, user) {
                    authenticate(null, null, function (err, aToken, aUserId) {
                        createProjectsFixture(null, null, null, null, null, function (err, dbProject) {
                            token = aToken;
                            projectId = dbProject.id;
                            done();
                        });
                    });
                });
            });
        });

        describe("GET", function () {

            describe("Wrong project ID format", function () {
                it("should return 400", function (done) {
                    chai.request(server)
                        .get("/api/projects/12345/roles")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong project ID", function () {
                it("should return 404", function (done) {
                    chai.request(server)
                        .get("/api/projects/123456789012345678901234/roles")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("No roles in the project", function () {
                it("should return an empty list", function (done) {
                    chai.request(server)
                        .get("/api/projects/" + projectId + "/roles")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(0);
                            done();
                        });
                });
            });

            describe("One role in the project", function () {

                beforeEach(function (done) {
                    createProjRolesFixture(projectId, function (err) {
                        done();
                    });
                });

                it("should return one role", function (done) {
                    chai.request(server)
                        .get("/api/projects/" + projectId + "/roles")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(1);
                            res.body[0].name.should.eql("Developer");
                            done();
                        });
                });
            });
        });

        describe("POST", function () {

            describe("Wrong project ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .post("/api/projects/12345/roles")
                        .set("x-access-token", token)
                        .send({name: "new role", project_id: projectId, description: "new description"})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong project ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .post("/api/projects/123456789012345678901234/roles")
                        .set("x-access-token", token)
                        .send({name: "new role", project_id: projectId, description: "new description"})
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Create a project role", function () {
                it("creates the role", function (done) {
                    chai.request(server)
                        .post("/api/projects/" + projectId + "/roles")
                        .set("x-access-token", token)
                        .send({
                            name: "new role",
                            project_id: projectId,
                            description: "new description",
                            title: "Web Developer"
                        })
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.name.should.exist;
                            res.body.id.should.exist;
                            res.body.name.should.eql("new role");
                            res.body.title.should.eql("Web Developer");
                            done();
                        });
                });
            });

            describe("Existing role", function () {

                var role = null;

                beforeEach(function (done) {
                    createProjRolesFixture(projectId, function (err, dbRole) {
                        role = dbRole;
                        done();
                    });
                });

                it("returns 409", function (done) {
                    chai.request(server)
                        .post("/api/projects/" + projectId + "/roles")
                        .set("x-access-token", token)
                        .send({name: role.name, project_id: projectId, description: "new description"})
                        .end(function (err, res) {
                            res.should.have.status(409);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("New role with no name", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .post("/api/projects/" + projectId + "/roles")
                        .set("x-access-token", token)
                        .send({project_id: projectId, description: "a new role"})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });
    });
});
