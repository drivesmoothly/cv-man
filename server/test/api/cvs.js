"use strict";

process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");
chai.use(chaiHttp);

var should = chai.should();
var server = require("../../app");

var cleanDb = require("../../testutils/clean-db");
var createUsersFixture = require("../../testutils/users-fixture");
var authenticate = require("../../testutils/authenticate");
var createCvsFixture = require("../../testutils/cvs-fixture");
var skillCategoriesFixture = require("../../testutils/skill-categories-fixture");
var skillsFixture = require("../../testutils/skills-fixture");
var competenceFixture = require("../../testutils/competences-fixture");

describe("/api/cvs", function () {

    describe("No token", function () {

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/cvs")
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("POST", function () {

            var user = null;

            beforeEach(function (done) {
                cleanDb(function () {
                    createUsersFixture(null, null, null, function (err, dbUser) {
                        user = dbUser;
                        done();
                    });
                });
            });

            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/cvs")
                    .send({name: "Web Developer", user_id: user.id})
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var user = null;
        var token = null;

        beforeEach(function (done) {
            cleanDb(function () {
                createUsersFixture(null, null, null, function (err, dbUser) {
                    authenticate(null, null, function (err, authToken) {
                        user = dbUser;
                        token = authToken;
                        done();
                    });
                });
            });
        });

        describe("GET", function () {

            describe("Empty list", function () {

                it("returns an empty list", function (done) {
                    chai.request(server)
                        .get("/api/cvs")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(0);
                            done();
                        });
                });
            });

            describe("A sample CV", function () {

                beforeEach(function (done) {
                    createCvsFixture(null, user.id, null, null, function (err, dbCv) {
                        done();
                    });
                });

                describe("A simple request", function () {
                    it("returns a list with one item", function (done) {
                        chai.request(server)
                            .get("/api/cvs")
                            .set("x-access-token", token)
                            .end(function (err, res) {
                                res.should.have.status(200);
                                res.body.should.be.a("array");
                                res.body.length.should.eql(1);
                                res.body[0].name.should.eql("Web Developer");
                                done();
                            });
                    });
                });

                describe("With query for another user", function () {
                    it("returns an empty list", function (done) {
                        chai.request(server)
                            .get("/api/cvs?user_id=123456789012345678901234")
                            .set("x-access-token", token)
                            .end(function (err, res) {
                                res.should.have.status(200);
                                res.body.should.be.a("array");
                                res.body.length.should.eql(0);
                                done();
                            });
                    });
                });

                describe("With query for the same user", function () {
                    it("returns an empty list", function (done) {
                        chai.request(server)
                            .get("/api/cvs?user_id=" + user.id)
                            .set("x-access-token", token)
                            .end(function (err, res) {
                                res.should.have.status(200);
                                res.body.should.be.a("array");
                                res.body.length.should.eql(1);
                                res.body[0].name.should.eql("Web Developer");
                                done();
                            });
                    });
                });
            });
        });

        describe("POST", function () {

            describe("Create a CV", function () {
                it("creates the cv", function (done) {
                    var cv_name = "Secondary - Qt Dev";
                    chai.request(server)
                        .post("/api/cvs")
                        .set("x-access-token", token)
                        .send({name: cv_name, user_id: user.id, job_title: "Qt Developer"})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.id.should.exist;
                            res.body.name.should.eql(cv_name);
                            res.body.job_title.should.eql("Qt Developer");
                            done();
                        });
                });
            });

            describe("User does not exist", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .post("/api/cvs")
                        .set("x-access-token", token)
                        .send({name: "dev", user_id: "123456789012345678901234", job_title: "Developer"})
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Role exists for the same user", function () {

                beforeEach(function (done) {
                    createCvsFixture("dev", user.id, null, null, function (err, newCv) {
                        done();
                    });
                });

                it("returns 409", function (done) {
                    chai.request(server)
                        .post("/api/cvs")
                        .set("x-access-token", token)
                        .send({name: "dev", user_id: user.id, job_title: "Developer"})
                        .end(function (err, res) {
                            res.should.have.status(409);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Role exists for a different user", function () {

                beforeEach(function (done) {
                    createUsersFixture("Ionica", "ionica@gmail.com", null, function (err, newUser) {
                        createCvsFixture("dev", newUser.id, null, null, function (err, newCv) {
                            done();
                        });
                    });
                });

                it("creates the CV", function (done) {
                    chai.request(server)
                        .post("/api/cvs")
                        .set("x-access-token", token)
                        .send({name: "dev", user_id: user.id, job_title: "Developer"})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.id.should.exist;
                            res.body.name.should.eql("dev");
                            res.body.job_title.should.eql("Developer");
                            done();
                        });
                });
            });
        });
    });
});

describe("/api/cvs/:id", function () {

    var cv = null;
    var user = null;

    beforeEach(function (done) {
        cleanDb(function (err) {
            createUsersFixture(null, null, null, function (err, dbUser) {
                skillCategoriesFixture(null, null, function (err, category) {
                    skillsFixture(null, category.id, function (err, skill) {
                        createCvsFixture(null, dbUser.id, null, skill.id, function (err, dbCv) {
                            user = dbUser;
                            cv = dbCv;
                            done();
                        });
                    });
                });
            });
        });
    });

    describe("No token", function () {

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/cvs/" + cv.id)
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("PUT", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .put("/api/cvs/" + cv.id)
                    .send({job_title: "not authenticated"})
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var token = null;

        beforeEach(function (done) {
            authenticate(null, null, function (err, authToken) {
                token = authToken;
                done();
            });
        });

        describe("GET", function () {

            describe("Retrieves a CV", function () {
                it("gets all the fields", function (done) {
                    chai.request(server)
                        .get("/api/cvs/" + cv.id)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.id.should.eql(cv.id);
                            res.body.job_title.should.eql("Web Developer");
                            res.body.name.should.eql("Web Developer");
                            res.body.office_location.should.eql("Cluj Napoca");
                            res.body.competences.should.be.a("array");
                            res.body.competences.length.should.eql(1);
                            res.body.competences[0].experience.percentage.should.eql(100);
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .get("/api/cvs/12345")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.eql("Wrong ID format");
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .get("/api/cvs/123456789012345678901234")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });

        describe("PUT", function () {

            describe("Update a role", function () {
                it("updates the role", function (done) {
                    chai.request(server)
                        .put("/api/cvs/" + cv.id)
                        .set("x-access-token", token)
                        .send({name: "new name", job_title: "Another title"})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.name.should.eql("new name");
                            res.body.job_title.should.eql("Another title");
                            done();
                        });
                });
            });

            describe("Cannot change user_id", function () {

                var ionica = null;

                beforeEach(function (done) {
                    createUsersFixture("ionica", "ionica@gmail.com", "oparola", function (err, dbIonica) {
                        ionica = dbIonica;
                        done();
                    });
                });

                it("returns 200 but does not update user_id", function (done) {
                    chai.request(server)
                        .put("/api/cvs/" + cv.id)
                        .set("x-access-token", token)
                        .send({user_id: ionica.id})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.user_id.should.eql(user.id);
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .put("/api/cvs/12345")
                        .set("x-access-token", token)
                        .send({job_title: "new title"})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .put("/api/cvs/123456789012345678901234")
                        .set("x-access-token", token)
                        .send({job_title: "new title"})
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });

        describe("DELETE", function () {

            describe("Deletes a CV", function () {
                it("deletes a CV", function (done) {
                    chai.request(server)
                        .delete("/api/cvs/" + cv.id)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.success.should.eql(true);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .delete("/api/cvs/12345")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .delete("/api/cvs/123456789012345678901234")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });
    });
});

describe("/api/cvs/:id/competences", function () {

    var cv = null;
    var user = null;
    var category = null;
    var skill = null;
    var competence = null;

    beforeEach(function (done) {
        cleanDb(function (err) {
            createUsersFixture(null, null, null, function (err, dbUser) {
                user = dbUser;
                skillCategoriesFixture(null, null, function (err, dbCategory) {
                    category = dbCategory;
                    skillsFixture(null, dbCategory, function (err, dbSkill) {
                        skill = dbSkill;
                        createCvsFixture(null, user.id, null, skill.id, function (err, dbCv) {
                            cv = dbCv;
                            competenceFixture(cv.id, skill.id, null, function (err, dbCompetence) {
                                competence = dbCompetence;
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

    describe("No token", function () {

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/cvs/" + cv.id + "/expertise")
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("POST", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .post("/api/cvs/" + cv.id + "/expertise")
                    .send({skill_id: skill.id, experience: {months: 36}})
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var token = null;

        beforeEach(function (done) {
            authenticate(null, null, function (err, aToken) {
                token = aToken;
                done();
            });
        });

        describe("GET", function () {

            describe("One competence", function () {
                it("returns 200", function (done) {
                    chai.request(server)
                        .get("/api/cvs/" + cv.id + "/competences")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(1);
                            done();
                        });
                });
            });
        });

        describe("POST", function () {

            describe("Create an competence", function () {

                var newSkill = null;

                beforeEach(function (done) {
                    skillsFixture("a new competence", null, function (err, dbNewSkill) {
                        newSkill = dbNewSkill;
                        done();
                    });
                });

                it("returns 200", function (done) {
                    chai.request(server)
                        .post("/api/cvs/" + cv.id + "/competences")
                        .set("x-access-token", token)
                        .send({skill_id: newSkill.id, experience: {months: 24}})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.skill_id.should.eql(newSkill.id);
                            res.body.experience.months.should.eql(24);
                            done();
                        });
                });
            });

            describe("Cannot create 2 competences on the same skill", function () {
                it("returns 409", function (done) {
                    chai.request(server)
                        .post("/api/cvs/" + cv.id + "/competences")
                        .set("x-access-token", token)
                        .send({skill_id: skill.id, experience: {months: 24}})
                        .end(function (err, res) {
                            res.should.have.status(409);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Skill must exist", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .post("/api/cvs/" + cv.id + "/competences")
                        .set("x-access-token", token)
                        .send({experience: {months: 24}})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });
    });
});
