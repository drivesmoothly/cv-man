"use strict";

process.env.NODE_ENV = "test";

var chai                    = require("chai");
var chaiHttp                = require("chai-http");
chai.use(chaiHttp);

var server                  = require("../../app");
var should                  = chai.should();

var cleanDb                 = require("../../testutils/clean-db");
var createUsersFixture      = require("../../testutils/users-fixture");
var authenticate            = require("../../testutils/authenticate");
var createCvsFixture        = require("../../testutils/cvs-fixture");

function clearAndAuthenticate(next) {
    cleanDb(function (err) {
        createUsersFixture(null, null, null, function (err, dbUser) {
            authenticate(null, null, function (err, aToken) {
                next(aToken, dbUser.id);
            });
        });
    });
}

describe("/api/users", function () {

    describe("No token", function () {

        function validateUnauthorized(res) {
            res.should.have.status(403);
            res.body.success.should.equal(false);
            res.body.message.should.equal("No token provided");
        }

        describe("GET", function () {
            it("returns a 403 error", function (done) {
                chai.request(server)
                    .get("/api/users")
                    .end(function (err, res) {
                        validateUnauthorized(res);
                        done();
                    });
            });
        });

        describe("POST", function () {
            it("returns a 403 error", function (done) {
                chai.request(server)
                    .post("/api/users")
                    .end(function (err, res) {
                        validateUnauthorized(res);
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var token = "";

        beforeEach(function (done) {
            clearAndAuthenticate(function (aToken) {
                token = aToken;
                done();
            });
        });

        describe("GET", function () {

            describe("A sample user", function () {
                it("returns only the dummy user", function (done) {
                    chai.request(server)
                        .get("/api/users")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.be.eql(1);
                            res.body[0].email.should.be.eql("johnny.bravo@ro.neusoft.com");
                            done();
                        });
                });
            });
        });

        describe("POST", function () {

            describe("Create a user", function () {
                it("creates a new user", function (done) {
                    chai.request(server)
                        .post("/api/users")
                        .set("x-access-token", token)
                        .send({displayname: "user1", email: "user1@ro.neusoft.com", auth: {local: {password: "pass1"}}})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.id.should.be.not.null;
                            res.body.displayname.should.be.eql("user1");
                            res.body.email.should.be.eql("user1@ro.neusoft.com");
                            done();
                        });
                });
            });

            describe("No email", function () {
                it("returns a 400 code", function (done) {
                    chai.request(server)
                        .post("/api/users")
                        .set("x-access-token", token)
                        .send({displayname: "user1", auth: {local: {password: "pass1"}}})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.be.eql(false);
                            done();
                        });
                });
            });
        });

    });
});

describe("/api/users/:id", function () {

    var token = "";
    var userId = "";

    beforeEach(function (done) {
        clearAndAuthenticate(function (aToken, aUserId) {
            token = aToken;
            userId = aUserId;
            done();
        });
    });

    describe("No token", function () {

        describe("GET", function () {
            it("returns a 403 status", function (done) {
                chai.request(server)
                    .get("/api/users/" + userId)
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("POST", function () {
            it("returns a 403 status", function (done) {
                chai.request(server)
                    .post("/api/users/" + userId)
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        describe("GET", function () {

            describe("Get sample user", function() {
                it("retrieves the user", function (done) {
                    chai.request(server)
                        .get("/api/users/" + userId)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.displayname.should.eql("Johnny Bravo");
                            res.body.email.should.eql("johnny.bravo@ro.neusoft.com");
                            done();
                        });
                });
            });

            describe("Wrong id format", function() {
                it("returns a 400 status", function (done) {
                    chai.request(server)
                        .get("/api/users/a-wrong-id")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong id", function() {
                it("returns a 404 status", function (done) {
                    chai.request(server)
                        .get("/api/users/1234567890abcdef01234567")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });

        describe("PUT", function () {

            describe("Update a user", function() {
                it("changes the user", function (done) {
                    chai.request(server)
                        .put("/api/users/" + userId)
                        .set("x-access-token", token)
                        .send({displayname: "Johnny B."})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.displayname.should.eql("Johnny B.");
                            res.body.email.should.eql("johnny.bravo@ro.neusoft.com");
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .put("/api/users/12345")
                        .set("x-access-token", token)
                        .send({displayname: "Gica"})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .put("/api/users/123456789012345678901234")
                        .set("x-access-token", token)
                        .send({displayname: "Gheo"})
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });

        describe("DELETE", function () {

            describe("Delete a user", function () {
                it("deletes the user", function (done) {
                    chai.request(server)
                        .delete("/api/users/" + userId)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.success.should.eql(true);
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .delete("/api/users/12345")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .delete("/api/users/123456789012345678901234")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });
    });
});

describe("/api/users/:id/cvs", function () {

    var user = null;
    var cv = null;

    beforeEach(function (done) {
        cleanDb(function (err) {
            createUsersFixture(null, null, null, function (err, dbUser) {
                user = dbUser;
                createCvsFixture(null, dbUser.id, null, null, function (err, dbCv) {
                    cv = dbCv;
                    done();
                });
            });
        });
    });

    describe("No token", function () {

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/users/" + user.id + "/cvs")
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var token = null;
        var other_user = null;

        // 2 users, one CV for each
        beforeEach(function (done) {
            createUsersFixture("another user", "anotheruser@gmail.com", "somepassword", function (err, dbUser) {
                other_user = dbUser;
                createCvsFixture("main cv", dbUser.id, "Director", null, function (err, dbCv) {
                    authenticate(null, null, function (err, aToken) {
                        token = aToken;
                        done();
                    });
                });
            });
        });

        describe("GET", function () {

            describe("Get the CVs for a user", function () {
                it("returns a CV", function (done) {
                    chai.request(server)
                        .get("/api/users/" + user.id + "/cvs")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(1);
                            res.body[0].user_id.should.eql(user.id);
                            done();
                        });
                });
            });

            describe("Invalid user id", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .get("/api/users/12345/cvs")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });
    });
});
