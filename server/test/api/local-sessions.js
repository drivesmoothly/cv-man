"use strict";

process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");
var should = chai.should();
var server = require("../../app");

var cleanDb = require("../../testutils/clean-db");
var createUsersFixture = require("../../testutils/users-fixture");

chai.use(chaiHttp);

describe("/api/localsession", function () {

    describe("POST", function () {

        var user = null;

        beforeEach(function (done) {
            cleanDb(function (err) {
                createUsersFixture(null, null, null, function (err, dbUser) {
                    user = dbUser;
                    done();
                });
            });
        });

        describe("Authenticate", function () {
            it("returns a token", function (done) {
                chai.request(server)
                    .post("/api/localsession")
                    .send({email: user.email, password: "johnny1"})
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.body.success.should.eql(true);
                        res.body.message.should.exist;
                        res.body.token.should.exist;
                        done();
                    });
            });
        });

        describe("User not found", function () {
            it("returns 401", function (done) {
                chai.request(server)
                    .post("/api/localsession")
                    .send({email: "usernotfound@gmail.com", password: "somepassword"})
                    .end(function (err, res) {
                        res.should.have.status(401);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("Wrong password", function () {
            it("returns 401", function (done) {
                chai.request(server)
                    .post("/api/localsession")
                    .send({email: user.email, password: "wrongpassword"})
                    .end(function (err, res) {
                        res.should.have.status(401);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });
});
