"use strict";

process.env.NODE_ENV = "test";

var chai = require("chai");
var chaiHttp = require("chai-http");
chai.use(chaiHttp);

var should = chai.should();
var server = require("../../app");

var opts = {
    chai: chai,
    chaiHttp: chaiHttp,
    server: server
}

var cleanDb = require("../../testutils/clean-db");
var createUsersFixture = require("../../testutils/users-fixture");
var authenticate = require("../../testutils/authenticate");
var createSkillCategoriesFixture = require("../../testutils/skill-categories-fixture");
var verify_token = require("../../testutils/verify-token")(opts);

describe("/api/skillcategories", function () {

    describe("No token", function () {

        describe("GET", function () {
            verify_token.verify_get("/api/skillcategories");
        });

        describe("POST", function () {
            verify_token.verify_post("/api/skillcategories", {name: "Technical", parent: null});
        });
    });

    describe("Authenticated", function () {

        var user = null;
        var token = null;

        beforeEach(function (done) {
            cleanDb(function () {
                createUsersFixture(null, null, null, function (err, dbUser) {
                    user = dbUser;
                    authenticate(null, null, function (err, authToken) {
                        token = authToken;
                        done();
                    });
                });
            });
        });

        describe("GET", function () {

            describe("Empty list", function () {
                it("returns an empty list", function (done) {
                    chai.request(server)
                        .get("/api/skillcategories")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(0);
                            done();
                        });
                });
            });

            describe("A sample category", function () {

                var skillCategory = null;

                beforeEach(function (done) {
                    createSkillCategoriesFixture(null, null, function (err, dbCategory) {
                        skillCategory = dbCategory;
                        done();
                    });
                });

                describe("A simple request", function () {
                    it("returns a list with one item", function (done) {
                        chai.request(server)
                            .get("/api/skillcategories")
                            .set("x-access-token", token)
                            .end(function (err, res) {
                                res.should.have.status(200);
                                res.body.should.be.a("array");
                                res.body.length.should.eql(1);
                                res.body[0].name.should.eql("Technical");
                                done();
                            });
                    });
                });
            });

            describe("Get only the top level categories", function () {

                beforeEach(function (done) {
                    createSkillCategoriesFixture("Technical", null, function (err, category1) {
                        createSkillCategoriesFixture("Programming Languages", "Technical,", function (err, category2) {
                            done();
                        });
                    });
                });

                it("returns only the top level category", function (done) {
                    chai.request(server)
                        .get("/api/skillcategories?path=")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.should.be.a("array");
                            res.body.length.should.eql(1);
                            res.body[0].name.should.eql("Technical");
                            done();
                        });
                });
            });
        });

        describe("POST", function () {

            describe("Creates a category", function () {
                it("creates the category", function (done) {
                    chai.request(server)
                        .post("/api/skillcategories")
                        .set("x-access-token", token)
                        .send({name: "Management"})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.id.should.exist;
                            res.body.name.should.eql("Management");
                            done();
                        });
                });
            });

            describe("Name is missing", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .post("/api/skillcategories")
                        .set("x-access-token", token)
                        .send({path: "Technical,"})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });
    });
});

describe("/api/skillcategories/:id", function () {

    var category = null;

    beforeEach(function (done) {
        cleanDb(function (err) {
            createSkillCategoriesFixture(null, null, function (err, dbCategory) {
                category = dbCategory;
                done();
            });
        });
    });

    describe("No token", function () {

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/skillcategories/" + category.id)
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("PUT", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .put("/api/skillcategories/" + category.id)
                    .send({name: "new name"})
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });

        describe("DELETE", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .delete("/api/skillcategories/" + category.id)
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var user = null;
        var token = null;

        beforeEach(function (done) {
            createUsersFixture(null, null, null, function (err, dbUser) {
                user = dbUser;
                authenticate(null, null, function (err, authToken) {
                    token = authToken;
                    done();
                });
            });
        });

        describe("GET", function () {

            describe("Retrieve a category", function () {
                it("returns a category", function (done) {
                    chai.request(server)
                        .get("/api/skillcategories/" + category.id)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.name.should.eql("Technical");
                            res.body.path.should.eql("");
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .get("/api/skillcategories/123456")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .get("/api/skillcategories/123456789012345678901234")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });

        describe("PUT", function () {

            describe("Change a category", function () {
                it("changes the category", function (done) {
                    chai.request(server)
                        .put("/api/skillcategories/" + category.id)
                        .set("x-access-token", token)
                        .send({name: "new name", path: "Some node,"})
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.name.should.eql("new name");
                            res.body.path.should.eql("Some node,");
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .put("/api/skillcategories/13245")
                        .set("x-access-token", token)
                        .send({name: "new name", path: "Some node,"})
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .put("/api/skillcategories/132456789012345678901234")
                        .set("x-access-token", token)
                        .send({name: "new name", path: "Some node,"})
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });

        describe("DELETE", function () {

            describe("Deletes a category", function () {
                it("deletes the category", function (done) {
                    chai.request(server)
                        .delete("/api/skillcategories/" + category.id)
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(200);
                            res.body.success.should.eql(true);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID format", function () {
                it("returns 400", function (done) {
                    chai.request(server)
                        .delete("/api/skillcategories/12345")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(400);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });

            describe("Wrong ID", function () {
                it("returns 404", function (done) {
                    chai.request(server)
                        .delete("/api/skillcategories/123456789012345678901234")
                        .set("x-access-token", token)
                        .end(function (err, res) {
                            res.should.have.status(404);
                            res.body.success.should.eql(false);
                            res.body.message.should.exist;
                            done();
                        });
                });
            });
        });
    });
});

describe("/api/skillcategories/:id/children", function () {

    var parent = null;
    var child = null;

    beforeEach(function (done) {
        cleanDb(function (err) {
            createSkillCategoriesFixture("technical", null, function (err, dbParent) {
                parent = dbParent;
                createSkillCategoriesFixture("languages", "technical,", function (err, dbChild) {
                    child = dbChild;
                    createSkillCategoriesFixture("other", null, function (err, dbOther) {
                        done();
                    });
                });
            });
        });
    });

    describe("No token", function () {

        describe("GET", function () {
            it("returns 403", function (done) {
                chai.request(server)
                    .get("/api/skillcategories/" + parent.id + "/children")
                    .end(function (err, res) {
                        res.should.have.status(403);
                        res.body.success.should.eql(false);
                        res.body.message.should.exist;
                        done();
                    });
            });
        });
    });

    describe("Authenticated", function () {

        var user = null;
        var token = null;

        beforeEach(function (done) {
            createUsersFixture(null, null, null, function (err, dbUser) {
                authenticate(null, null, function (err, authToken) {
                    user = dbUser;
                    token = authToken;
                    done();
                });
            });
        });

        describe("GET", function () {
            it("returns a list with one item", function (done) {
                chai.request(server)
                    .get("/api/skillcategories/" + parent.id + "/children")
                    .set("x-access-token", token)
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.body.should.be.a("array");
                        res.body.length.should.eql(1);
                        res.body[0].name.should.eql("languages");
                        done();
                    });
            });
        });
    });
});
