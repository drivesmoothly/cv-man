"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var User = mongoose.model("user", new Schema({
    displayname: Schema.Types.String,
    email: Schema.Types.String,
    birthdate: Schema.Types.Date,
    auth: {
        ldap: {
            enabled: Schema.Types.Bool
        },
        local: {
            enabled: Schema.Types.Bool,
            password: Schema.Types.String
        }
    }
}));

module.exports = User;
