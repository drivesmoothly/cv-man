
"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Role = mongoose.model("project-role", new Schema({
    name: Schema.Types.String,
    project_id: Schema.Types.ObjectId,
    title: Schema.Types.String,
    description: Schema.Types.String
}));

module.exports = Role;
