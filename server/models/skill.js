"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var skillSchema = new Schema({
    name: Schema.Types.String,
    category_id: Schema.Types.ObjectId,
    path: Schema.Types.String
});
skillSchema.index({name: 1}, {unique: true});

module.exports = mongoose.model("skill", skillSchema);
