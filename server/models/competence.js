"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Competence = mongoose.model("competence", new Schema({
    cv_id: {type: Schema.Types.ObjectId, ref: "cv"},
    skill_id: Schema.Types.ObjectId,
    experience: {
        months: Schema.Types.Number,
        since: Schema.Types.Date,
        percentage: Schema.Types.Number,
        description: Schema.Types.String
    }
}));

module.exports = Competence;
