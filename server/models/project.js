
"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Project = mongoose.model("project", new Schema({
    name: Schema.Types.String,
    shortdescription: Schema.Types.String,
    fulldescription: Schema.Types.String,
    startdate: Schema.Types.Date,
    enddate: Schema.Types.Date
}));

module.exports = Project;
