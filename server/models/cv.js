"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Cv = mongoose.model("cv", new Schema({
    name: Schema.Types.String,
    user_id: Schema.Types.ObjectId,
    job_title: Schema.Types.String,
    office_location: Schema.Types.String,
    competences: [
        {
            skill_id: Schema.Types.ObjectId,
            experience: {
                months: Schema.Types.Number,
                since: Schema.Types.Date,
                percentage: Schema.Types.Number
            }
        }
    ]
}));

module.exports = Cv;
