"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var SkillCategorySchema = new Schema({
    name: Schema.Types.String,
    path: Schema.Types.String
});
SkillCategorySchema.index({name: 1, path: 1}, {unique: true});

var SkillCategory = mongoose.model("skill-category", SkillCategorySchema);

module.exports = SkillCategory;
