
"use strict";

function hasOwnPropertyNested(obj /*, level1, level2, ... */) {
    var args = Array.prototype.slice.call(arguments, 1);
    var result = true;

    args.every(function (property, index, array) {
        if (!obj || (obj[property] === undefined)) {
            result = false;
            return false;
        }
        obj = obj[property];
        return true;
    });

    return result;
}

module.exports = hasOwnPropertyNested;
